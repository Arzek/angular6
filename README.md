# Abm new LK

# Install
1. Install **node.js** and **npm** (**node.js** > 10)
2. `npm install`
3. Rename `.env.example` to `.env` and set Project name

# Build
1. `npm run build`

# Project maintenance
## And project
1. Projects folder - `/src/projects`
2. Copy `/src/projects/dev` to `/src/projects/{ProjectName}`
3. `/src/projects/{ProjectName}/config/config.ts` - config
4. `/src/projects/{ProjectName}/config/theme.scss` - view var
5. `/src/projects/public` - static files (image and doc),(build in `/dist/assets/project/`, url = `/assets/project/`)
6. Change var `Project`(on Project name) in `.env`
7. `npm run build`
## Update project
1. `git pull`
2. `npm run build`
## Checkout project
1. Change var `Project`(on Project name) in `.env`
2. `npm run build`