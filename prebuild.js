require('dotenv').config();
const fs = require('fs');
const fsExtra = require('fs-extra');

const project = process.env.PROJECT;

/** theme **/
fs.readFile(`./src/projects/${project}/config/theme.scss`, 'utf8', function (err, data) {
  fs.writeFile('./src/theme.scss', data, 'utf8', function (err) {
    if (err) return console.log(err);
  });
});


/** environments **/
let environments =
`export const environment = {
  production: true,
  projectName: '${project}'
};`;

fs.writeFile('./src/environments/environment.prod.ts', environments, 'utf8', function (err) {
  if (err) return console.log(err);
});

/** config **/
fs.readFile(`./src/projects/${project}/config/config.ts`, 'utf8', function (err, data) {
  fs.writeFile('./src/config.ts', data, 'utf8', function (err) {
    if (err) return console.log(err);
  });
});

/** project public data **/
fsExtra.copy(`./src/projects/${project}/public`,`./src/assets/project`, function (err) {
  if (err) return console.log(err);
});
