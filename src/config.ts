export const staticConfig = {
  apiUrl: 'https://api.lotok.ua',
  phone: {
    regex: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12,14}(\s*)?$/,
    mask: ['+', '3', '8', '0', ' ' , '(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    showMask: true
  },
  minPassword: 6,
  fullRegistration: true,
  countryId: 1,
  minimumYear: 14,
  suport: {
    phone: '0 800 508 504',
    email: 'call.center@lotok.in.ua',
  },
  title: 'LOTOK',
  social: [
    {
      show: true,
      icon: 'ico-social-fb',
      link: 'https://www.facebook.com/lotok.ukraine'
    },
    {
      show: false,
      icon: 'ico-social-ok',
      link: 'https://ok.ru/dk?st.cmd=anonymMain&st.redirect=%2Ffeed'
    },
    {
      show: false,
      icon: 'ico-social-vk',
      link: 'https://m.vk.com/pivo_eliseeff'
    },
    {
      show: false,
      icon: 'ico-social-in',
      link: 'https://www.instagram.com/elisey_supermarket/'
    },
  ],
  regulationsLink: '/assets/project/regulations.pdf',
  linkLogo: 'https://my.lotok.ua/',
  titleLogo: 'Lotok',
  androidLink: 'https://play.google.com/store/apps/details?id=pro.mwallet.lotok',
  iosLink: 'https://itunes.apple.com/us/app/лоток/id1142672475',
  requiredCard: true,

  lang: {
    currentLang: {
      name: 'UKR',
      alias: 'ukr',
      header: 'uk'
    },
    languages: [
      {
        name: 'UKR',
        alias: 'ukr',
        header: 'uk'
      },
      {
        name: 'RU',
        alias: 'ru',
        header: 'ru'
      },
      {
        name: 'ENG',
        alias: 'eng',
        header: 'en'
      }
    ]
  },
  showWelcomeSection: true
};
