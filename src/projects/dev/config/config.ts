export const staticConfig = {
  apiUrl: 'https://api.dev.m-reward.com',
  phone: {
    regex: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12,14}(\s*)?$/,
    mask: ['+', '3', '8', '0', ' ' , '(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    showMask: true
  },
  minPassword: 6,
  fullRegistration: true,
  countryId: 1,
  minimumYear: 14,
  suport: {
    phone: '+3801232132',
    email: 'email@email.com',
  },
  title: 'Title',
  social: [
    {
      show: true,
      icon: 'ico-social-fb',
      link: 'https://www.facebook.com/grosh.family'
    },
    {
      show: true,
      icon: 'ico-social-ok',
      link: 'https://ok.ru/dk?st.cmd=anonymMain&st.redirect=%2Ffeed'
    },
    {
      show: true,
      icon: 'ico-social-vk',
      link: 'https://m.vk.com/pivo_eliseeff'
    },
    {
      show: true,
      icon: 'ico-social-in',
      link: 'https://www.instagram.com/elisey_supermarket/'
    },
  ],
  regulationsLink: '/assets/project/regulations.txt',
  linkLogo: 'http://my.grosh.vin.ua',
  titleLogo: 'ABM Loyalty',
  androidLink: 'addroid',
  iosLink: 'ios',
  requiredCard: true,

  lang: {
    currentLang: {
      name: 'UKR',
      alias: 'ukr',
      header: 'uk'
    },
    languages: [
      {
        name: 'UKR',
        alias: 'ukr',
        header: 'uk'
      },
      {
        name: 'RU',
        alias: 'ru',
        header: 'ru'
      },
      {
        name: 'ENG',
        alias: 'eng',
        header: 'en'
      }
    ]
  },
  showWelcomeSection: true
};
