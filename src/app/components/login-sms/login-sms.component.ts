import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InputText } from '../../models/input-text.store';
import { LoginService } from '../../services/login.service';
import { EnableProgressAction, DisableProgressAction, SetErrorInputTextAction } from '../../store/actions/app.action';
import { AppStore } from '../../models/app.store';
import { ErrorModel } from '../../models/error-action.model';
import { StorageService } from '../../services/storage.service';
import { Navigate } from '@ngxs/router-plugin';

@Component({
  selector: 'app-login-sms',
  templateUrl: './login-sms.component.html',
  styleUrls: ['./login-sms.component.scss']
})
export class LoginSmsComponent extends BaseComponent implements OnInit {

  @Select(state => state.app.phone) phone$: Observable<InputText>;
  @Select(state => state.app) appStore$: Observable<AppStore>;

  step = 1;
  smsId = null;
  constructor(private store: Store, private service: LoginService, private storage: StorageService) {
    super();
  }

  onSubmit() {
    this.appStore$.subscribe( (store: AppStore) => {
      this.store.dispatch(new EnableProgressAction());
      this.service.loginPhone(store.phone.value).then(res => {
        this.store.dispatch(new DisableProgressAction());
        this.smsId = res.sms_id;
        this.step = 2;
      });
    });
  }

  confirmSMS(code) {
    this.appStore$.subscribe( (store: AppStore) => {
      this.store.dispatch(new EnableProgressAction());

      this.service.phoneConfirm(store.phone.value, code, this.smsId).then(res => {
        this.storage.setItem('token', res.token).then(() => {
          this.store.dispatch([
            new DisableProgressAction(),
            new Navigate(['/profile'])
          ]);
        });
      }).catch(error => {
        this.store.dispatch(new DisableProgressAction());
        for (const item of error.error.data) {
          if (item.field === 'code' || item.field === 'sms_id') {
            this.store.dispatch(new SetErrorInputTextAction(
              new ErrorModel({
                field: 'sms',
                isTextError: true,
                message: item.message
              })
            ));
          }
        }
      });


    });
  }

  ngOnInit() {
  }

}
