import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BaseStepsRegistrationComponent } from '../base-steps-registration.component';
import { HttpService } from '../../services/http.service';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { EnableProgressAction, DisableProgressAction, SetErrorInputTextAction, SetItemAction } from '../../store/actions/app.action';
import { Navigate } from '@ngxs/router-plugin';
import { BaseComponent } from '../base.component';
import { InputText } from '../../models/input-text.store';
import { AppStore } from '../../models/app.store';
import { ErrorModel } from '../../models/error-action.model';
import { RegistrationService } from '../../services/registration.service';
import { StorageService } from '../../services/storage.service';
import { CartService } from '../../services/cart.service';
import { KeyValue } from '../../models/key-value';
import { SetUserSubscribe } from '../../store/actions/profile.action';


@Component({
  selector: 'app-full-registration',
  templateUrl: './full-registration.component.html',
  styleUrls: ['./full-registration.component.scss']
})
export class FullRegistrationComponent extends BaseComponent implements OnInit {

  profileData = null;
  regions = null;
  data = {};
  profileFields = null;
  step = 1;
  smsId = null;

  @Select(state => state.app) appStore$: Observable<AppStore>;
  @Select(state => state.app.cart) cart$: Observable<InputText>;
  @Select(state => state.app.needPassword) needPassword$: Observable<boolean>;
  @Select(state => state.app.passwordOne) passwordOne$: Observable<InputText>;
  @Select(state => state.app.passwordTwo) passwordTwo$: Observable<InputText>;
  @Select(state => state.app.phone) phone$: Observable<InputText>;

  constructor(private http: HttpService,
    private store: Store,
    private cdr: ChangeDetectorRef,
    private service: RegistrationService,
    private storage: StorageService,
    private cartService: CartService
  ) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new EnableProgressAction());
    this.http.get('/client/profile-params').toPromise().then(data => {
      this.profileData = data;
      this.store.dispatch(new DisableProgressAction());
    });

    this.store.dispatch(new EnableProgressAction());
    this.http.get(`/client/geo/${this.staticConfig.countryId}/regions`).toPromise().then(data => {
      this.regions = data.target;
      this.store.dispatch(new DisableProgressAction());
    });

    this.store.dispatch(new EnableProgressAction());
    this.http.get(`/system/profile-fields`).toPromise().then(data => {
      this.profileFields = data;
      this.store.dispatch(new DisableProgressAction());
    });
  }

  setDataItem(value: any, field: string) {
    if (value && value !== '') {
      this.data[field] = value;
    } else {
      delete this.data[field];
    }
    console.log(this.data);
  }

  chackPasswords() {
    this.appStore$.subscribe((store: AppStore) => {
      if (store.passwordOne.valid.status && store.passwordTwo.valid.status) {
        if (store.passwordOne.value !== store.passwordTwo.value) {
          this.store.dispatch(new SetErrorInputTextAction(
            new ErrorModel({
              field: 'passwordTwo',
              isCodeError: true,
              code: 126
            })
          ));
        }
      }
    });
  }

  valid() {
    if (this.profileData && this.profileFields) {
      // valid profileData
      for (const  key in this.profileData['params']['required']) {
        if (key in this.profileData['params']['required']) {
          if (this.profileData['params']['required'][key]) {
            if (key !== 'mobile') {
              if (!(key in this.data)) {
                return false;
              }
            }
          }
        }
      }

      // valid profileFields

      for (const item of this.profileFields.fields) {
        if (item.required) {
          if (!(item.key in this.data)) {
            return false;
          }
        }
      }

      return true;
    } else {
      return false;
    }
  }

  submit() {
    this.appStore$.subscribe((store: AppStore) => {
      if (store.needPassword) {
        this.service.registration(store.phone.value, store.passwordOne.value).then(res => {
          this.store.dispatch(new DisableProgressAction());
          this.smsId = res.sms_id;
          this.step = 2;
        }).catch(error => {
          this.store.dispatch(new DisableProgressAction());
          for (const item of error.error.data) {
            this.store.dispatch(new SetErrorInputTextAction(
              new ErrorModel({
                field: item.field,
                isTextError: true,
                message: item.message
              })
            ));
          }
        });
      } else {
        this.service.registrationPhone(store.phone.value).then(res => {
          this.store.dispatch(new DisableProgressAction());
          this.smsId = res.sms_id;
          this.step = 2;
        }).catch(error => {
          this.store.dispatch(new DisableProgressAction());
          for (const item of error.error.data) {
            this.store.dispatch(new SetErrorInputTextAction(
              new ErrorModel({
                field: item.field,
                isTextError: true,
                message: item.message
              })
            ));
          }
        });
      }
    });
  }

  confirmSMS(code) {
    this.store.dispatch(new EnableProgressAction());

    this.service.registrationConfirm(code, this.smsId).then(res => {

      this.storage.setItem('token', res.token).then(() => {

        this.appStore$.subscribe((store: AppStore) => {

          this.cartService.setCart(store.cart.value).then(() => {

            this.store.dispatch(new EnableProgressAction());

            this.service.editProfile(this.data).then(() => {
              this.store.dispatch([new DisableProgressAction(), new SetUserSubscribe(), new Navigate(['/profile'])]);
            }).catch(error => error);

          }).catch(error => {
            this.store.dispatch(new DisableProgressAction());
            for (const item of error.error.data) {
              this.store.dispatch(new SetErrorInputTextAction(
                new ErrorModel({
                  field: item.field,
                  isTextError: true,
                  message: item.message
                })
              ));
            }
          });
        });
      });

    }).catch(error => {
      this.store.dispatch(new DisableProgressAction());
      for (const item of error.error.data) {
        if (item.field === 'code' || item.field === 'sms_id') {
          this.store.dispatch(new SetErrorInputTextAction(
            new ErrorModel({
              field: 'sms',
              isTextError: true,
              message: item.message
            })
          ));
        }
      }
    });
  }


  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }

}
