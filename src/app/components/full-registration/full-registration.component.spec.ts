import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullRegistrationComponent } from './full-registration.component';

describe('FullRegistrationComponent', () => {
  let component: FullRegistrationComponent;
  let fixture: ComponentFixture<FullRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
