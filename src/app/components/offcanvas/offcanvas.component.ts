import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-offcanvas',
  templateUrl: './offcanvas.component.html',
  styleUrls: ['./offcanvas.component.scss']
})
export class OffcanvasComponent extends BaseComponent implements OnInit {

  auth = false;
  constructor(private authService: AuthService) {
    super();
  }

  ngOnInit() {
    this.auth = this.authService.isAuth();
  }

}
