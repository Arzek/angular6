import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../services/http.service';
import { Store } from '@ngxs/store';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import * as moment from 'moment';

@Component({
  selector: 'app-actions-page',
  templateUrl: './actions-page.component.html',
  styleUrls: ['./actions-page.component.scss']
})
export class ActionsPageComponent extends BaseComponent implements OnInit {

  item: Object;
  constructor(private route: ActivatedRoute, private http: HttpService, private store: Store) {
    super();
  }

  format(dateStr) {
    return moment(dateStr).format('DD/MM/YYYY');
  }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.store.dispatch(new EnableProgressAction());
    this.item = await this.http.get(`/client/partner/actions/${id}`).toPromise();
    this.store.dispatch(new DisableProgressAction());
  }

}
