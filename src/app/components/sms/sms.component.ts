import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { BaseComponent } from '../base.component';
import { ErrorInput } from '../../models/error-input.model';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InputText } from '../../models/input-text.store';
import { SetSuccessInputTextAction, SetErrorInputTextAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss']
})
export class SmsComponent extends BaseComponent {
  @Output() send: EventEmitter<string> = new EventEmitter<string>();
  @Output() readonly = false;
  @Input() mode = 'login';
  @Select(state => state.app.sms) sms$: Observable<InputText>;
  confirm: boolean;
  model = null;
  constructor(private store: Store) {
    super();
  }
  onUpdate() {
    if (this.mode === 'registration') {
      console.log(this.model, this.confirm);
      if (this.model && this.confirm) {
        this.store.dispatch(new SetSuccessInputTextAction('sms'));
      } else {
        this.store.dispatch(new SetErrorInputTextAction(
          new ErrorModel({
            field: 'sms',
            isBooleanError: true
          })
        ));
      }
    } else if (this.mode === 'login') {
      if (this.model) {
        this.store.dispatch(new SetSuccessInputTextAction('sms'));
      } else {
        this.store.dispatch(new SetErrorInputTextAction(
          new ErrorModel({
            isBooleanError: true
          })
        ));
      }
    }
  }

  onCheck() {
    if (this.model) {
      this.store.dispatch(new SetSuccessInputTextAction('sms'));
    } else {
      this.store.dispatch(new SetErrorInputTextAction(
        new ErrorModel({
          isBooleanError: true
        })
      ));
    }
  }
  onSend() {
    this.send.emit(this.model);
  }
}
