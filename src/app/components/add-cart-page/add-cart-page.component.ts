import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { CartService } from '../../services/cart.service';
import { CardHelper } from '../../helpers/card.helper';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InputText } from '../../models/input-text.store';
import { EnableProgressAction, DisableProgressAction, SetErrorInputTextAction } from '../../store/actions/app.action';
import { Navigate } from '@ngxs/router-plugin';
import { ErrorModel } from '../../models/error-action.model';
import { ErrorHelper } from '../../helpers/error.helper';

@Component({
  selector: 'app-add-cart-page',
  templateUrl: './add-cart-page.component.html',
  styleUrls: ['./add-cart-page.component.scss']
})
export class AddCartPageComponent extends BaseComponent implements OnInit {
  status: boolean;
  @Select(state => state.app.cart) cart$: Observable<InputText>;
  constructor(private servive: CartService, private helper: CardHelper, private store: Store, private errorHelper: ErrorHelper) {
    super();
  }

  async ngOnInit() {
    this.store.dispatch(new EnableProgressAction());
    const cards = await this.servive.getCards();
    if (!cards.length || !this.helper.valid(cards)) {
      this.status = true;
    } else {
      this.status = false;
    }
    this.store.dispatch([new DisableProgressAction()]);
  }

  setCard() {
    this.store.dispatch(new EnableProgressAction());
    this.cart$.subscribe(res => {
      this.servive.setCart(res.value).then(() => {
        this.store.dispatch([new DisableProgressAction(), new Navigate(['/profile'])]);
      }).catch(error => {
        this.store.dispatch([new DisableProgressAction(),
        new SetErrorInputTextAction(
          new ErrorModel({
            field: 'cart',
            isTextError: true,
            message: this.errorHelper.getMassege(error)
          })
        )]);
      });
    });
  }

}
