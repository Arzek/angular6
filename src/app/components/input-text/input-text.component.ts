import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base.component';
import { InputText } from '../../models/input-text.store';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent extends BaseComponent implements OnInit {

  @Input() readonly = false;
  @Input() tabindex = null;
  @Input() required = false;
  @Input() placeholderCode = null;
  @Input() placeholder = null;
  @Input() type = 'text';
  @Input() value = null;
  @Output() update: EventEmitter<string> = new EventEmitter<string>();
  inputText: InputText;

  constructor() {
    super();
  }
  ngOnInit() {
    if (this.required) {
      this.inputText = new InputText();
    } else {
      this.inputText = new InputText(true);
    }

    if (this.value) {
      this.inputText.value = this.value;
      this.update.emit(this.value);
      this.inputText.valid.clearError();
    }
  }
  onChange() {
    this.update.emit(this.inputText.value);
    if (this.required) {
      if (this.inputText.value) {
        this.inputText.valid.clearError();
      } else {
        this.inputText.valid.setErrorNoMassage();
      }
    }
  }
}
