import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '../base.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-category]',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent extends BaseComponent implements OnInit {

  @Input() data = {};
  mode = 'right';

  constructor() {
    super();
  }

  toggleMode() {
    if (this.mode === 'right') {
      this.mode = 'down';
    } else if (this.mode === 'down') {
      this.mode = 'right';
    }
  }

  ngOnInit() {
  }

}
