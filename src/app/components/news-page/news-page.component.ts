import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../services/http.service';
import { Store } from '@ngxs/store';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss']
})
export class NewsPageComponent extends BaseComponent implements OnInit {

  item: Object;
  constructor(private route: ActivatedRoute, private http: HttpService, private store: Store) { 
    super();
  }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const partnerId = this.route.snapshot.paramMap.get('partnerId');

    this.store.dispatch(new EnableProgressAction());
    this.item = await this.http.get(`/client/partner/${partnerId}/news/${id}`).toPromise();
    this.store.dispatch(new DisableProgressAction());
  }

}
