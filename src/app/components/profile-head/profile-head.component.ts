import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { BaseComponent } from '../base.component';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProfileStore } from '../../models/profile.store';
import { ProfileModel } from '../../models/profile.model';

@Component({
  selector: 'app-profile-head',
  templateUrl: './profile-head.component.html',
  styleUrls: ['./profile-head.component.scss']
})
export class ProfileHeadComponent extends BaseComponent implements OnInit {

  @Select(state => state.profile.profileData) profileData$: Observable<ProfileModel>;

  constructor(public auth: AuthService) {
    super();
  }

  ngOnInit() {
  }

}
