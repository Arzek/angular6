import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base.component';
import { InputText } from '../../models/input-text.store';
import * as moment from 'moment';

@Component({
  selector: 'app-input-datepicker',
  templateUrl: './input-datepicker.component.html',
  styleUrls: ['./input-datepicker.component.scss']
})
export class InputDatepickerComponent extends BaseComponent implements OnInit {

  @Input() placeholderCode = null;
  @Input() placeholder = null;
  @Input() required  = false;
  @Input() ageLimit = true;
  @Input() value = null;
  @Input() disable = false;
  @Input() mode = 1;
  @Output() update: EventEmitter<any> = new EventEmitter<any>();
  @Output() updateDate: EventEmitter<any> = new EventEmitter<any>();

 model: InputText;

  constructor() {
    super();
  }
  onValueChange(value) {
    if (value) {
      if (this.ageLimit) {
        if (this.getCurrentAge(new Date(value)) < this.staticConfig.minimumYear) {
          this.model.valid.setError(109);
        } else {
          this.model.valid.clearError();
          this.update.emit(moment(value).format('YYYY-MM-DD'));
          this.updateDate.emit(value);
        }
      } else {
        this.update.emit(moment(value).format('YYYY-MM-DD'));
        this.updateDate.emit(value);
        this.model.valid.clearError();
      }
    }
  }

  ngOnInit() {
    if (this.required) {
      this.model = new InputText();
    } else {
      this.model = new InputText(true);
    }

    if (this.value) {
      this.update.emit(moment(this.value).format('YYYY-MM-DD'));
      this.updateDate.emit(this.value);

      this.value = new Date(this.value);
      this.model.value = this.value;
      this.model.valid.clearError();
    }
  }

  getCurrentAge(date): number {
    // tslint:disable-next-line:no-bitwise
    return ((new Date().getTime() - date) / (24 * 3600 * 365.25 * 1000)) | 0;
  }

}
