import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { LangEntity } from '../../models/lang.entity';
import { ChangeLangAction } from '../../store/actions/lang.action';
import { BaseComponent } from '../base.component';

import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';

declare var UIkit: any;

@Component({
  selector: 'app-language-widget',
  templateUrl: './language-widget.component.html',
  styleUrls: ['./language-widget.component.scss']
})
export class LanguageWidgetComponent extends BaseComponent implements OnInit {
  constructor(private store: Store, private localeService: BsLocaleService) {
    super();
    if (localStorage.getItem('lang')) {
      this.localeService.use(localStorage.getItem('lang'));
    } else {
      this.localeService.use(this.staticConfig.lang.currentLang.header);
    }
  }
  ngOnInit() {
  }

  changeLang(item: LangEntity) {
    this.store.dispatch(new ChangeLangAction(item));
    this.localeService.use(item.header);
  }

}
