import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProfileModel } from '../../models/profile.model';
import { HttpService } from '../../services/http.service';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import { RegistrationService } from '../../services/registration.service';

@Component({
  selector: 'app-mailing-page',
  templateUrl: './mailing-page.component.html',
  styleUrls: ['./mailing-page.component.scss']
})
export class MailingPageComponent extends BaseComponent implements OnInit {

  @Select(state => state.profile.profileData) profileData$: Observable<ProfileModel>;
  sms = false;
  email = false;
  constructor(private http: HttpService, private store: Store, private service: RegistrationService) {
    super();
  }

  ngOnInit() {
    this.store.dispatch(new EnableProgressAction());
    this.http.get('/client/profile').toPromise().then(res => {
      this.store.dispatch(new DisableProgressAction());
      this.sms = res['sms_subscribe'];
      this.email = res['email_subscribe'];
    });
  }

  onChange() {
    this.store.dispatch(new EnableProgressAction());
    this.service.editProfile({
      sms_subscribe: Number(this.sms),
      email_subscribe: Number(this.email)
    }).then(res => {
      this.store.dispatch(new DisableProgressAction());
    });
  }

}
