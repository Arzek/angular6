import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable } from '../../../../node_modules/rxjs';
import { ForgotPasswordActions } from '../../store/actions/forgot-password.action';
import { KeyValue } from '../../models/key-value';
import { Navigate } from '../../../../node_modules/@ngxs/router-plugin';
import { Input } from '@angular/compiler/src/core';
import { InputText } from '../../models/input-text.store';
import { AppStore } from '../../models/app.store';
import { SetErrorInputTextAction, EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';
import { ForgotPasswordService } from '../../services/forgot-password.service';
import { ErrorHelper } from '../../helpers/error.helper';

@Component({
  selector: 'app-forgot-password-page',
  templateUrl: './forgot-password-page.component.html',
  styleUrls: ['./forgot-password-page.component.scss']
})
export class ForgotPasswordPageComponent extends BaseComponent {

  @Select(state => state.app) appStore$: Observable<AppStore>;
  @Select(state => state.app.passwordOne) passwordOne$: Observable<InputText>;
  @Select(state => state.app.passwordTwo) passwordTwo$: Observable<InputText>;
  @Select(state => state.app.phone) phone$: Observable<InputText>;

  step = 1;
  smsId = null;
  constructor(private store: Store, private service: ForgotPasswordService, private errorHelper: ErrorHelper) {
    super();
  }

  chackPasswords() {
    this.appStore$.subscribe((store: AppStore) => {
      if (store.passwordOne.valid.status && store.passwordTwo.valid.status) {
        if (store.passwordOne.value !== store.passwordTwo.value) {
          this.store.dispatch(new SetErrorInputTextAction(
            new ErrorModel({
              field: 'passwordTwo',
              isCodeError: true,
              code: 126
            })
          ));
        }
      }
    });
  }

  submitStepOne() {
    this.store.dispatch(new EnableProgressAction());
    this.appStore$.subscribe((store: AppStore) => {
      this.service.changePassword(store.phone.value, store.passwordOne.value).then((data) => {
        this.store.dispatch(new DisableProgressAction());
        this.smsId = data.sms_id;
        this.step = 2;
      });
    });
  }

  sendSmsPassword(password) {
    this.store.dispatch(new EnableProgressAction());
    this.service.changePasswordConfirm(this.smsId, password).then(res => {
      this.store.dispatch([new DisableProgressAction(), new Navigate(['/login'])]);
    }).catch(error => {
      this.store.dispatch([new DisableProgressAction(), new SetErrorInputTextAction(
        new ErrorModel({
          field: 'sms',
          isTextError: true,
          message: this.errorHelper.getMassege(error)
        })
      )]);
    });
  }
}
