import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { ConfigStore } from '../../models/config.store';
import { GetNeedPassword } from '../../store/actions/login.action';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent extends BaseComponent implements OnInit {

  @Select(store => store.login.needPassword) needPassword$: Observable<boolean>;
  needPassword: boolean;

  constructor(private store: Store) {
    super();
    this.store.dispatch(new GetNeedPassword());
  }

  ngOnInit() {
    this.needPassword$.subscribe(needPassword => this.needPassword = needPassword);
  }

}
