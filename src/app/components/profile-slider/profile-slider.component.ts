import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '../base.component';
import * as moment from 'moment';

@Component({
  selector: 'app-profile-slider',
  templateUrl: './profile-slider.component.html',
  styleUrls: ['./profile-slider.component.scss']
})
export class ProfileSliderComponent extends BaseComponent {

  @Input() name = '';
  @Input() title = '';
  @Input() list = [];
  constructor() {
    super();
  }

  previous() {
    document.getElementById(`${this.name}_previous`).click();
  }
  next() {
    document.getElementById(`${this.name}_next`).click();
  }

  getLinkNews(partnerId, id) {
    return `/profile/news/${partnerId}/${id}`;
  }

  getLinkAction(id) {
    return `/profile/actions/${id}`;
  }

  getDate(time) {
    return moment.unix(time).format('DD/MM/YYYY');
  }
}
