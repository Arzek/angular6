import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoStepRegistrationComponent } from './two-step-registration.component';

describe('TwoStepRegistrationComponent', () => {
  let component: TwoStepRegistrationComponent;
  let fixture: ComponentFixture<TwoStepRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoStepRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoStepRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
