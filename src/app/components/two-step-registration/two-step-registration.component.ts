import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { BaseStepsRegistrationComponent } from '../base-steps-registration.component';
import { HttpService } from '../../services/http.service';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import { Navigate } from '@ngxs/router-plugin';
import { RegistrationService } from '../../services/registration.service';
import { SetUserSubscribe } from '../../store/actions/profile.action';

@Component({
  selector: 'app-two-step-registration',
  templateUrl: './two-step-registration.component.html',
  styleUrls: ['./two-step-registration.component.scss'],
  // changeDetection: ChangeDetectionStrategy.,
})
export class TwoStepRegistrationComponent extends BaseStepsRegistrationComponent implements OnInit {
  @Input() codeSubmitBtn = 5;
  @Input() disableBirthDay = false;
  profileData = null;
  regions = null;
  data = {};
  profileFields = null;

  constructor(private http: HttpService, private store: Store, private cdr: ChangeDetectorRef, private service: RegistrationService) {
    super();
  }

  async ngOnInit() {
    this.store.dispatch(new EnableProgressAction());

    this.profileData =  await this.http.get('/client/profile').toPromise();
    const regionRes = await this.http.get(`/client/geo/${this.staticConfig.countryId}/regions`).toPromise();
    this.regions = regionRes.target;

    this.profileFields = await this.http.get(`/system/profile-fields`).toPromise();

    this.store.dispatch(new DisableProgressAction());
  }

  setDataItem(value: any, field: string) {
    if (value && value !== '') {
      this.data[field] = value;
    } else {
      delete this.data[field];
    }
    console.log(this.data);
  }

  valid() {
    if (this.profileData && this.profileFields) {
      // valid profileData
      for (const  key in this.profileData['params']['required']) {
        if (key in this.profileData['params']['required']) {
          if (this.profileData['params']['required'][key]) {
            if (key !== 'mobile') {
              if (!(key in this.data)) {
                return false;
              }
            }
          }
        }
      }

      // valid profileFields

      for (const item of this.profileFields.fields) {
        if (item.required) {
          if (!(item.key in this.data)) {
            return false;
          }
        }
      }

      return true;
    } else {
      return false;
    }
  }

  submit() {
    // /client/profile
    this.store.dispatch(new EnableProgressAction());
    this.service.editProfile(this.data).then(res => {
      if (!this.disableBirthDay) {
        this.store.dispatch(new SetUserSubscribe());
      }

      this.store.dispatch([new DisableProgressAction(), new Navigate(['/profile'])]);
    }).catch(error => error);
  }


  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
}
