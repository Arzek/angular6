import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProfileService } from '../../services/profile.service';
import { DisableProgressAction, EnableProgressAction } from '../../store/actions/app.action';
import { GetProfileDataAction } from '../../store/actions/profile.action';

@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.scss']
})
export class FeedbackPageComponent extends BaseComponent implements OnInit {

  @Select(state => state.profile.questionsNew) questionsNew$: Observable<any[]>;
  @Select(state => state.profile.questionsOld) questionsOld$: Observable<any[]>;
  constructor(private servive: ProfileService, private store: Store) {
    super();
  }

  send(id: number, value: number, currentAnswer: any) {
    if (currentAnswer === null && currentAnswer !== 0) {
    this.store.dispatch(new EnableProgressAction());
      this.servive.setQuestion(id, value).then(() => {
        this.store.dispatch([new DisableProgressAction(), new GetProfileDataAction()]);
      });
    }
  }

  ngOnInit() {
  }

}
