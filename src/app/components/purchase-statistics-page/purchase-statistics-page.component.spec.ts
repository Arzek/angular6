import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseStatisticsPageComponent } from './purchase-statistics-page.component';

describe('PurchaseStatisticsPageComponent', () => {
  let component: PurchaseStatisticsPageComponent;
  let fixture: ComponentFixture<PurchaseStatisticsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchaseStatisticsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseStatisticsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
