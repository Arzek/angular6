import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Store } from '@ngxs/store';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import * as moment from 'moment';
import { BaseComponent } from '../base.component';


@Component({
  selector: 'app-purchase-statistics-page',
  templateUrl: './purchase-statistics-page.component.html',
  styleUrls: ['./purchase-statistics-page.component.scss']
})
export class PurchaseStatisticsPageComponent extends BaseComponent implements OnInit {

  fromSrt = '01.01.2015';
  partners = [];

  from;
  to;

  showChar = false;
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  barChartLabels: string[] = [];
  barChartType = 'bar';
  barChartLegend = true;

  barChartData: any[] = [];


  constructor(private http: HttpService, private store: Store) {
    super();
  }

  async ngOnInit() {
    this.store.dispatch(new EnableProgressAction());
    this.partners = (await this.http.get('/client/partners').toPromise()).items;
    for (const partner of this.partners) {
      this.barChartData.push({data: [], label: partner.company});
    }
    this.showChar = true;
    this.store.dispatch(new DisableProgressAction());

    this.setAllTime();
  }
  setAllTime() {
    this.from = new Date(this.fromSrt);
    this.to = new Date();
  }
  setYear() {
    const date = new Date();
    date.setFullYear(date.getFullYear() - 1);

    this.from = date;
    this.to = new Date();
  }

  setMount() {
    const date = new Date();
    date.setMonth(date.getMonth() - 1);

    this.from = date;
    this.to = new Date();
  }

  update() {

    for (const partner of this.partners) {
      /** labels */


      /** char  */
      // this.http.get(`/client/statistics/spent-in-time?` +
      // `period=months&partner_id=${partner.id}&from=${this.getFormat(this.from)}&to=${this.getFormat(this.to)}`)
      // .toPromise()
      // .then(res => {
      //   console.log(res);
      //   //debugger;
      // });

      /** products */
      this.http.get(`/client/statistics/products` +
      `?period=months&partner_id=${partner.id}&from=${this.getFormat(this.from)}&to=${this.getFormat(this.to)}`)
      .toPromise()
      .then(res => {
        if ('products' in res) {
          this.setData(partner.id, res);
        }
      });
    }
  }

  setData(partnerId, data) {
    this.partners = this.partners.map(item => {
      if (item.id === partnerId) {
        item.data = data.products;
        return item;
      } else {
        return item;
      }
    });
  }

  setFrom(date) {
    this.from = date;
    this.update();
  }

  setTo(date) {
    this.to = date;
    this.update();
  }

  private getFormat(date) {
    return moment(date).format('DD.MM.YYYY');
  }

}
