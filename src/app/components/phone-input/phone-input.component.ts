import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { BaseComponent } from '../base.component';
import { PhoneHelper } from '../../helpers/phone.helper';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InputText } from '../../models/input-text.store';
import { SetErrorInputTextAction, SetSuccessInputTextAction, SetValueInputTextAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';
import { PhoneService } from '../../services/phone.service';
import { ErrorHelper } from '../../helpers/error.helper';
import { KeyValue } from '../../models/key-value';

@Component({
  selector: 'app-phone-input',
  templateUrl: './phone-input.component.html',
  styleUrls: ['./phone-input.component.scss']
})
export class PhoneInputComponent extends BaseComponent {
  @Input() readonly = false;
  @Input() mode = 'login';
  @Input() tabindex = null;
  @Select(state => state.app.phone) phone$: Observable<InputText>;


  model: string;
  constructor(private store: Store, private service: PhoneService, private errorHelper: ErrorHelper) {
    super();
  }
  onChange() {
    if (this.preValid()) {
      if (this.mode === 'registration') {
        this.onChangeRegistration();
      } else if (this.mode === 'login') {
        this.onChangeLogin();
      }
    }
  }
  private preValid(): boolean {
    if (this.model) {
      if (this.staticConfig.phone.regex.test(this.model)) {
        return true;
      } else {
        this.store.dispatch(
          new SetErrorInputTextAction(
            new ErrorModel({ field: 'phone', isCodeError: true, code: 130 })
          )
        );
        return false;
      }
    } else {
      this.store.dispatch(
        new SetErrorInputTextAction(
          new ErrorModel({ field: 'phone', isBooleanError: true })
        )
      );
      return false;
    }
  }

  private onChangeLogin() {
    this.service.validPhone(this.model).then(res => {
      if (res.is_exist) {
        this.setSuccess();
      } else {
        this.store.dispatch(
          new SetErrorInputTextAction(
            new ErrorModel({ field: 'phone', isCodeError: true, code: 106 })
          )
        );
      }
    }).catch(error => this.setServerError(error));
  }

  private onChangeRegistration() {
    this.service.validPhone(this.model).then(res => {
      if (res.is_exist) {
        this.store.dispatch(
          new SetErrorInputTextAction(
            new ErrorModel({ field: 'phone', isCodeError: true, code: 101 })
          )
        );
      } else {
        this.setSuccess();
      }
    }).catch(error => this.setServerError(error));
  }

  private setServerError(error) {
    this.store.dispatch(
      new SetErrorInputTextAction(
        new ErrorModel({ field: 'phone', isTextError: true, message: this.errorHelper.getMassege(error) })
      )
    );
  }

  private setSuccess () {
    this.store.dispatch([
      new SetValueInputTextAction(new KeyValue('phone', this.model)),
      new SetSuccessInputTextAction('phone')]);
  }
}
