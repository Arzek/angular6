import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputRegionComponent } from './input-region.component';

describe('InputRegionComponent', () => {
  let component: InputRegionComponent;
  let fixture: ComponentFixture<InputRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputRegionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
