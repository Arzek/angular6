import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputText } from '../../models/input-text.store';
import { Store } from '@ngxs/store';
import { SetItemAction } from '../../store/actions/app.action';
import { KeyValue } from '../../models/key-value';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-input-region',
  templateUrl: './input-region.component.html',
  styleUrls: ['./input-region.component.scss']
})
export class InputRegionComponent extends BaseComponent implements OnInit {

  @Input() list: any[] = [];
  @Input() value = null;
  @Output() update: EventEmitter<string> = new EventEmitter<string>();
  model: InputText = new InputText();
  constructor(private store: Store) {
    super();
  }
  setItem(id) {
    this.model.value = id;
    this.update.emit(this.model.value);
  }
  get name() {
    if (this.model.value) {
      return this.list.find(item => item.region_id === this.model.value).region_name;
    } else {
      return '';
    }
  }
  ngOnInit() {
    //this.emit( this.list[0].region_id);
    if (this.value) {
      this.update.emit(this.value);
      this.model.value = this.value;
      this.model.valid.clearError();
    }
  }
  emit(id) {
    this.update.emit(id);
  }
}
