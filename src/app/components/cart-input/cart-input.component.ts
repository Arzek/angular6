import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base.component';
import { ErrorInput } from '../../models/error-input.model';
import { CartService } from '../../services/cart.service';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { InputText } from '../../models/input-text.store';
import { SetErrorInputTextAction, SetSuccessInputTextAction, SetValueInputTextAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';
import { ErrorHelper } from '../../helpers/error.helper';
import { KeyValue } from '../../models/key-value';

@Component({
  selector: 'app-cart-input',
  templateUrl: './cart-input.component.html',
  styleUrls: ['./cart-input.component.scss']
})
export class CartInputComponent extends BaseComponent {
  @Input() readonly = false;
  @Select(state => state.app.cart) cart$: Observable<InputText>;
  model: string;
  constructor(private cartService: CartService, private store: Store, private errorHelper: ErrorHelper) {
    super();
   }
  onChange() {
    if (this.model) {
      this.cartService.validCart(this.model).then(res => {
        if (res.availability) {
          this.store.dispatch([
            new SetValueInputTextAction(new KeyValue('cart', this.model)),
            new SetSuccessInputTextAction('cart')]);
        } else {
          this.store.dispatch(
            new SetErrorInputTextAction(
              new ErrorModel({
                field: 'cart',
                isCodeError: true,
                code: 100
              })
            )
          );
        }
      }).catch(error => {
        this.store.dispatch(
          new SetErrorInputTextAction(
            new ErrorModel({ field: 'cart', isTextError: true, message: this.errorHelper.getMassege(error) })
          )
        );
      });
    } else {
      this.store.dispatch(
        new SetErrorInputTextAction(
          new ErrorModel({
            field: 'cart',
            isBooleanError: true
          })
        )
      );
    }
  }

}
