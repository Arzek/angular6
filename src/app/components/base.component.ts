import { Store, Select } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { LangModel } from '../models/lang.model';
import {staticConfig} from '../../config';
import { OnInit } from '@angular/core';
import * as moment from 'moment';

export class BaseComponent {
  @Select(store => store.lang.model) lang$: Observable<LangModel>;
  staticConfig = staticConfig;

  getDateInUnix(time) {
    return moment.unix(time).format('DD/MM/YYYY');
  }

  checkCategories(data) {
    if ('categories' in data) {
      return true;
    } else {
      return false;
    }
  }

  checkProducts(data) {
    if ('products' in data) {
      return true;
    } else {
      return false;
    }
  }

  in(field, data) {
    if (field in data) {
      return true;
    } else {
      return false;
    }
  }
}
