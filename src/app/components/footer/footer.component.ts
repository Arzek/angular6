import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent extends BaseComponent implements OnInit {

  social = [];
  constructor() {
    super();
  }

  ngOnInit() {
    this.social = this.staticConfig.social.filter(item => item.show === true);
  }

}
