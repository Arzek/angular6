import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputText } from '../../models/input-text.store';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.scss']
})
export class InputSelectComponent extends BaseComponent implements OnInit {

  @Input() list: any[] = [];
  @Input() required = false;
  @Input() placeholderCode = null;
  @Input() placeholder = null;
  @Input() nameFiled = 'name';
  @Input() value = null;
  @Output() update: EventEmitter<string> = new EventEmitter<string>();
  model: InputText;

  constructor() {
    super();
  }

  setItem(id) {
    this.model.value = id;
    this.update.emit(this.model.value);

    if (this.required && id === '0') {
      this.model.valid.setErrorNoMassage();
    } else {
      this.model.valid.clearError();
    }
  }
  get name(): string {
    if (this.model.value) {
      // tslint:disable-next-line:triple-equals
      return this.list.find(item => item.id == this.model.value)[this.nameFiled];
    }
  }
  ngOnInit() {
    if (this.required) {
      this.model = new InputText();
    } else {
      this.model = new InputText(true);
    }

    if (this.value) {
      this.model.value = this.value;
      this.update.emit(this.value);
      this.model.valid.clearError();
    }
  }

}
