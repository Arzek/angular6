import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputText } from '../../models/input-text.store';
import { HttpService } from '../../services/http.service';
import { staticConfig } from '../../../config';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-input-city',
  templateUrl: './input-city.component.html',
  styleUrls: ['./input-city.component.scss']
})
export class InputCityComponent extends BaseComponent implements OnInit {

  @Input() regionId = null;
  @Input() regions = [];
  @Input() placeholderCode = null;
  @Input() placeholder = null;
  @Input() list: any[] = [];
  @Input() value = null;
  @Output() update: EventEmitter<string> = new EventEmitter<string>();
  model: InputText = new InputText();

  constructor(private http: HttpService) {
    super();
  }

  setItem(name, id) {
    this.model.value = name;
    this.update.emit(id);
  }

  change() {
    if (this.model.value && this.regionId) {
      this.http.get(`/client/geo/${staticConfig.countryId}/${this.model.value}/search-city`)
      .toPromise()
      .then(res => {
        if (res.target) {
          this.list = res.target.filter(item => item.region_id === this.regionId);
        }
      });
    }
  }


  async ngOnInit() {
    if (this.value) {
      this.http.get(`/client/geo/${this.value}/get-city`).toPromise().then(res => {
        this.model.value = res.target.city_name;
        this.update.emit(this.value);
        this.model.valid.clearError();
      });
    }
  }
}
