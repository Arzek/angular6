import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent extends BaseComponent implements OnInit {
  auth = false;
  constructor(private store: Store, private service: AuthService) {
    super();
  }

  ngOnInit() {
    this.auth = this.service.isAuth();
  }
  toLogin() {
    this.store.dispatch(new Navigate(['/login']));
    this.ngOnInit();
  }
  toRegistration() {
    this.store.dispatch(new Navigate(['/registration']));
  }
}
