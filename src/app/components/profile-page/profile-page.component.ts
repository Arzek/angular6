import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent extends BaseComponent implements OnInit {

  constructor(public auth: AuthService) {
    super();
  }

  ngOnInit() {
  }

}
