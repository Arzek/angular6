import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSlidersComponent } from './profile-sliders.component';

describe('ProfileSlidersComponent', () => {
  let component: ProfileSlidersComponent;
  let fixture: ComponentFixture<ProfileSlidersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileSlidersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSlidersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
