import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-profile-sliders',
  templateUrl: './profile-sliders.component.html',
  styleUrls: ['./profile-sliders.component.scss']
})
export class ProfileSlidersComponent extends BaseComponent implements OnInit {

  news = [];
  actions = [];
  constructor(private http: HttpService) {
    super();
  }

  ngOnInit() {
    this.http.get('/client/partners').toPromise().then(res => {
      for (const item of res.items) {
        this.http.get(`/client/partner/${item.id}/news?sort=-date`).toPromise().then((newsPartner) => {
          if (newsPartner.items.length) {
            const news = newsPartner.items.map(partnerNews => {
              return {...partnerNews, partnerId: item.id};
            });
            this.news = [...this.news, ...news];
          }
          });
      }
    });

    this.http.get('/client/partner/actions?published=1&sort=-date').toPromise().then(res => {
      this.actions = res.items;
    });
  }

}
