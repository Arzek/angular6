import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BaseStepsRegistrationComponent } from '../base-steps-registration.component';
import { ErrorInput } from '../../models/error-input.model';
import { RegistrationService } from '../../services/registration.service';
import { InputText } from '../../models/input-text.store';
import { CartService } from '../../services/cart.service';
import { StorageService } from '../../services/storage.service';
import { BaseComponent } from '../base.component';
import { ForgotPasswordPageComponent } from '../forgot-password-page/forgot-password-page.component';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppStore } from '../../models/app.store';
import { EnableProgressAction, DisableProgressAction, SetErrorInputTextAction, SetItemAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';
import { KeyValue } from '../../models/key-value';

@Component({
  selector: 'app-one-step-registration',
  templateUrl: './one-step-registration.component.html',
  styleUrls: ['./one-step-registration.component.scss']
})
export class OneStepRegistrationComponent extends BaseComponent {

  @Select(state => state.app) appStore$: Observable<AppStore>;
  @Select(state => state.app.cart) cart$: Observable<InputText>;
  @Select(state => state.app.needPassword) needPassword$: Observable<boolean>;
  @Select(state => state.app.passwordOne) passwordOne$: Observable<InputText>;
  @Select(state => state.app.passwordTwo) passwordTwo$: Observable<InputText>;
  @Select(state => state.app.phone) phone$: Observable<InputText>;

  step = 1;
  smsId = null;

  constructor(private store: Store,
    private service: RegistrationService,
    private cartService: CartService,
    private storage: StorageService
  ) {
    super();
  }

  submitStepOne() {
    this.store.dispatch(new EnableProgressAction());
    this.appStore$.subscribe((store: AppStore) => {
      if (store.needPassword) {
        this.submitStepOnePassword(store.phone.value, store.passwordOne.value);
      } else {
        this.submitStepOneNoPassword(store.phone.value);
      }
    });
  }

  private submitStepOnePassword(phone: string, password: string) {
    this.service.registration(phone, password).then(res => {
      this.store.dispatch(new DisableProgressAction());
      this.smsId = res.sms_id;
      this.step = 2;
    }).catch(error => {
      this.store.dispatch(new DisableProgressAction());
      for (const item of error.error.data) {
        this.store.dispatch(new SetErrorInputTextAction(
          new ErrorModel({
            field: item.field,
            isTextError: true,
            message: item.message
          })
        ));
      }
    });
  }
  private submitStepOneNoPassword(phone) {
    this.service.registrationPhone(phone).then(res => {
      this.store.dispatch(new DisableProgressAction());
      this.smsId = res.sms_id;
      this.step = 2;
    }).catch(error => {
      this.store.dispatch(new DisableProgressAction());
      for (const item of error.error.data) {
        this.store.dispatch(new SetErrorInputTextAction(
          new ErrorModel({
            field: item.field,
            isTextError: true,
            message: item.message
          })
        ));
      }
    });
  }

  validStepOne() {
    return this.appStore$.subscribe((appStore: AppStore) => {
      return appStore.phone.valid.status;
    });
  }

  chackPasswords() {
    this.appStore$.subscribe((store: AppStore) => {
      if (store.passwordOne.valid.status && store.passwordTwo.valid.status) {
        if (store.passwordOne.value !== store.passwordTwo.value) {
          this.store.dispatch(new SetErrorInputTextAction(
            new ErrorModel({
              field: 'passwordTwo',
              isCodeError: true,
              code: 126
            })
          ));
        }
      }
    });
  }
  confirmSMS(code) {
    this.store.dispatch(new EnableProgressAction());
    this.service.registrationConfirm(code, this.smsId).then(res => {

      this.storage.setItem('token', res.token).then(() => {
        this.appStore$.subscribe((store: AppStore) => {
          this.cartService.setCart(store.cart.value).then(() => {
            this.store.dispatch([
              new DisableProgressAction(),
              new SetItemAction(new KeyValue('registrationLarge', true)),
              new SetItemAction(new KeyValue('step', 2))
            ]);
          }).catch(error => {
            this.store.dispatch(new DisableProgressAction());
            for (const item of error.error.data) {
              this.store.dispatch(new SetErrorInputTextAction(
                new ErrorModel({
                  field: item.field,
                  isTextError: true,
                  message: item.message
                })
              ));
            }
          });
        });
      });

    }).catch(error => {
      this.store.dispatch(new DisableProgressAction());
      for (const item of error.error.data) {
        if (item.field === 'code' || item.field === 'sms_id') {
          this.store.dispatch(new SetErrorInputTextAction(
            new ErrorModel({
              field: 'sms',
              isTextError: true,
              message: item.message
            })
          ));
        }
      }
    });
  }

}
