import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneStepRegistrationComponent } from './one-step-registration.component';

describe('OneStepRegistrationComponent', () => {
  let component: OneStepRegistrationComponent;
  let fixture: ComponentFixture<OneStepRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneStepRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneStepRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
