import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputText } from '../../models/input-text.store';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-input-checkbox',
  templateUrl: './input-checkbox.component.html',
  styleUrls: ['./input-checkbox.component.scss']
})
export class InputCheckboxComponent extends BaseComponent implements OnInit {

  @Input() readonly = false;
  @Input() tabindex = null;
  @Input() required = false;
  @Input() placeholderCode = null;
  @Input() placeholder = null;
  @Input() value = null;
  @Output() update: EventEmitter<string> = new EventEmitter<string>();

  model: InputText;
  constructor() {
    super();
  }
  onChange() {
    if (this.required) {
      if (this.model.value) {
        this.update.emit(this.model.value);
        this.model.valid.clearError();
      } else {
        this.update.emit(null);
        this.model.valid.setErrorNoMassage();
      }
    } else {
      this.update.emit(this.model.value);
      this.model.valid.clearError();
    }
  }

  ngOnInit() {
    if (this.required) {
      this.model = new InputText();
    } else {
      this.model = new InputText(true);
    }

    if (this.value) {
      this.model.value = this.value;
      this.update.emit(this.value);
      if (!this.required && this.value) {
        this.model.valid.clearError();
      }
    }
  }

}
