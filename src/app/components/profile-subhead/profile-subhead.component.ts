import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ProfileModel } from '../../models/profile.model';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-profile-subhead',
  templateUrl: './profile-subhead.component.html',
  styleUrls: ['./profile-subhead.component.scss']
})
export class ProfileSubheadComponent extends BaseComponent implements OnInit {

  @Select(state => state.profile.profileData) profileData$: Observable<ProfileModel>;
  @Select(state => state.profile.accountsData) accountsData$: Observable<ProfileModel>;
  @Select(state => state.profile.countQuestions) countQuestions$: Observable<number>;

  constructor(private http: HttpService) {
    super();
  }

  ngOnInit() {
  }

}
