import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSubheadComponent } from './profile-subhead.component';

describe('ProfileSubheadComponent', () => {
  let component: ProfileSubheadComponent;
  let fixture: ComponentFixture<ProfileSubheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileSubheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSubheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
