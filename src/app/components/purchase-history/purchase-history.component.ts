import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { BaseComponent } from '../base.component';
import * as moment from 'moment';
import { Store } from '@ngxs/store';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss']
})
export class PurchaseHistoryComponent extends BaseComponent implements OnInit {

  data = [];
  constructor(private http: HttpService, private store: Store) {
    super();
  }

  async ngOnInit() {
   const res = await this.http.get('/client/purchase-history').toPromise();
   this.data = res['purchases'];
  }

  async update(data) {
    const dateTo = moment(data[0]).format('YYYY-MM-DD');
    const dateFrom = moment(data[1]).format('YYYY-MM-DD');

    this.store.dispatch(new EnableProgressAction());
    const res = await this.http.get(`/client/purchase-history?dateFrom=${dateFrom}&dateTo=${dateTo}`).toPromise();
    this.data = res['purchases'];
    this.store.dispatch(new DisableProgressAction());
  }

  getShop(item) {
    if ('transactions' in item) {
      const res = item.transactions.find(findItem => {
        if (findItem.pay_type === 2) {
          return findItem;
        }
      });

      if (res) {
        return res.shop;
      } else {
        return item.transactions[0].shop;
      }
    }
  }
  getPayBonus(item) {
    if ('transactions' in item) {
      const res = item.transactions.find(findItem => {
        if (findItem.pay_type === 1) {
          return findItem;
        }
      });

      if (res) {
        return res.pay_bonus;
      } else {
        return item.transactions[0].pay_bonus;
      }
    }
  }
  getSum(item) {
    if ('transactions' in item) {
      const res = item.transactions.find(findItem => {
        if (findItem.pay_type === 2) {
          return findItem;
        }
      });

      if (res) {
        return res.pay_sum;
      } else {
        return item.transactions[0].pay_sum;
      }
    }
  }

  getBonus(item) {
    if ('transactions' in item) {
      const pay_type_2 = item.transactions.find(findItem => {
        if (findItem.pay_type === 2) {
          return findItem;
        }
      });

      const pay_type_1 = item.transactions.find(findItem => {
        if (findItem.pay_type === 1) {
          return findItem;
        }
      });

      if (pay_type_1 && pay_type_2) {
        return this.plus(pay_type_1.pay_bonus - pay_type_2.pay_bonus);
      } else if (pay_type_1 && !pay_type_2) {
        return this.plus(pay_type_1.pay_bonus);
      } else if (!pay_type_1 && pay_type_2){
        return this.plus(pay_type_2.pay_bonus);
      } else {
        return '';
      }
    }
  }

  getCheckPositions(item) {
    if ('transactions' in item) {
      const pay_type_2 = item.transactions.find(findItem => {
        if (findItem.pay_type === 2) {
          return findItem;
        }
      });

      if (pay_type_2) {
        if ('check_positions' in pay_type_2) {
          return pay_type_2.check_positions;
        } else {
          return [];
        }
      } else {
        const pay_type_1 = item.transactions.find(findItem => {
          if (findItem.pay_type === 1) {
            return findItem;
          }
        });

        if ('check_positions' in pay_type_1) {
          return pay_type_1.check_positions;
        } else {
          return [];
        }
      }

    }
  }

  getProductBonus(item) {
    const pay_type_2 = item.transactions.find(findItem => {
      if (findItem.pay_type === 2) {
        return findItem;
      }
    });

    if (pay_type_2) {
      return pay_type_2.pay_bonus;
    } else {
      return 0;
    }
  }

  getPaySumInPayType1(item) {
    const pay_type_1 = item.transactions.find(findItem => {
      if (findItem.pay_type === 1) {
        return findItem;
      }
    });

    if (pay_type_1) {
      return pay_type_1.pay_sum;
    } else {
      return 0;
    }
  }

  plus(number) {
    if (number > 0) {
      return '+' + number.toFixed(3);
    } else {
      return number.toFixed(3);
    }
  }

}
