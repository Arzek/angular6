import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputRangepickerComponent } from './input-rangepicker.component';

describe('InputRangepickerComponent', () => {
  let component: InputRangepickerComponent;
  let fixture: ComponentFixture<InputRangepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputRangepickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputRangepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
