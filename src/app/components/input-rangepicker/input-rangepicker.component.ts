import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-input-rangepicker',
  templateUrl: './input-rangepicker.component.html',
  styleUrls: ['./input-rangepicker.component.scss']
})
export class InputRangepickerComponent extends BaseComponent implements OnInit {

  @Input() placeholderCode = null;
  @Input() placeholder = null;
  @Input() required  = false;
  @Input() ageLimit = true;
  @Input() value = null;
  @Input() disable = false;
  @Input() mode = 1;
  @Output() update: EventEmitter<any> = new EventEmitter<any>();

  @Input() model = null;
  constructor() {
    super();
  }

  ngOnInit() {
  }
  onValueChange(data) {
    console.log(data);
    this.update.emit(data);
  }
}
