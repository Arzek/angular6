import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent extends BaseComponent {


  @Select(state => state.app.registrationLarge) registrationLarge$: Observable<boolean>;
  @Select(state => state.app.step) step$: Observable<number>;
  constructor() {
    super();
  }
}
