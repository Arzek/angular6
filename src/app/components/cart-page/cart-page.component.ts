import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CartService } from '../../services/cart.service';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import { GetProfileDataAction } from '../../store/actions/profile.action';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss']
})
export class CartPageComponent extends BaseComponent {

  @Select(state => state.profile.cards) cards$: Observable<any[]>;

  constructor(private servive: CartService, private store: Store) {
    super();
  }

  blockCard(card) {
    this.store.dispatch(new EnableProgressAction());
    this.servive.blockCard(card).then(() => {
      this.store.dispatch([new DisableProgressAction(), new GetProfileDataAction()]);
    });
  }

}
