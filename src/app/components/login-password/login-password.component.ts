import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { LangStore } from '../../models/lang.store';
import { LoginStore } from '../../models/login.store';
import { LoginPasswordAction, SetPasswortAction, SetPhoneAction } from '../../store/actions/login.action';
import { ErrorInput } from '../../models/error-input.model';
import { Navigate } from '../../../../node_modules/@ngxs/router-plugin';
import { InputText } from '../../models/input-text.store';
import { AppState } from '../../store/state/app.state';
import { LoginService } from '../../services/login.service';
import { AppStore } from '../../models/app.store';
import { dispatch } from 'rxjs/internal/observable/range';
import { StorageService } from '../../services/storage.service';
import { EnableProgressAction, DisableProgressAction, SetErrorInputTextAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';

@Component({
  selector: 'app-login-password',
  templateUrl: './login-password.component.html',
  styleUrls: ['./login-password.component.scss']
})
export class LoginPasswordComponent extends BaseComponent {

  @Select(state => state.app) appStore$: Observable<AppStore>;
  @Select(state => state.app.password) password$: Observable<InputText>;
  @Select(state => state.app.phone) phone$: Observable<InputText>;

  progress = false;

  constructor(private store: Store, private service: LoginService, private storage: StorageService) {
    super();
  }

  onSubmit() {
    this.appStore$.subscribe((store: AppStore) => {
      this.store.dispatch(new EnableProgressAction());
      this.service.loginPassword(store.phone.value, store.password.value).then(res => {
        this.storage.setItem('token', res.token).then(() => {
          this.store.dispatch(new DisableProgressAction());
          this.store.dispatch(new Navigate(['/profile']));
        });
      }).catch(error => {
        this.store.dispatch(new DisableProgressAction());
        for (const item of error.error.data) {
          this.store.dispatch(new SetErrorInputTextAction(
            new ErrorModel({
              field: item.field,
              isTextError: true,
              message: item.message
            })
          ));
        }
      });
    });
  }
  toForgotPassword() {
    this.store.dispatch(new Navigate(['forgot-password']));
  }
}
