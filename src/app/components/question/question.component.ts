import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Store } from '@ngxs/store';
import { ProfileService } from '../../services/profile.service';
import { EnableProgressAction, DisableProgressAction } from '../../store/actions/app.action';
import { GetProfileDataAction } from '../../store/actions/profile.action';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent extends BaseComponent implements OnInit {

  @Input() item;
  list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  constructor(private store: Store, private service: ProfileService) {
    super();
  }

  send(id: number, value: number, currentAnswer: any) {
    if (currentAnswer === null && currentAnswer !== 0) {
    this.store.dispatch(new EnableProgressAction());
      this.service.setQuestion(id, value).then(() => {
        this.store.dispatch([new DisableProgressAction(), new GetProfileDataAction()]);
      });
    }
  }

  ngOnInit() {
  }

}
