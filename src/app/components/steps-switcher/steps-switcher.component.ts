import { Component, OnInit, Input } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BaseComponent } from '../base.component';

@Component({
  selector: 'app-steps-switcher',
  templateUrl: './steps-switcher.component.html',
  styleUrls: ['./steps-switcher.component.scss']
})
export class StepsSwitcherComponent extends BaseComponent {

  @Select(state => state.app.step) step$: Observable<boolean>;
  constructor() {
    super();
  }
}
