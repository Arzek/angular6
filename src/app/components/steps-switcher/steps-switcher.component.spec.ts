import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepsSwitcherComponent } from './steps-switcher.component';

describe('StepsSwitcherComponent', () => {
  let component: StepsSwitcherComponent;
  let fixture: ComponentFixture<StepsSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepsSwitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepsSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
