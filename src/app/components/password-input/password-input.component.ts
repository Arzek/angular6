import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from '../base.component';
import { Store, Select } from '@ngxs/store';
import { ErrorInput } from '../../models/error-input.model';
import { Observable } from 'rxjs';
import { InputText } from '../../models/input-text.store';
import { SetErrorInputTextAction, SetSuccessInputTextAction, SetValueInputTextAction } from '../../store/actions/app.action';
import { ErrorModel } from '../../models/error-action.model';
import { KeyValue } from '../../models/key-value';
import { AppStore } from '../../models/app.store';

@Component({
  selector: 'app-password-input',
  templateUrl: './password-input.component.html',
  styleUrls: ['./password-input.component.scss']
})
export class PasswordInputComponent extends BaseComponent implements OnInit {
  @Input() readonly = false;
  @Input() tabindex = null;
  @Input() field: string;
  @Output() success: EventEmitter<null> = new EventEmitter<null>();
  @Select(state => state.app) appStore$: Observable<AppStore>;
  password: InputText;
  model: string;
  constructor(private store: Store) {
    super();
  }
  ngOnInit() {
    this.appStore$.subscribe((store: AppStore) => {
      this.password = store[this.field];
    });
  }
  onChange() {
    if (!this.model || this.model.length < this.staticConfig.minPassword) {
      this.store.dispatch(
        new SetErrorInputTextAction(
          new ErrorModel({field: this.field, isCodeError: true, code: 127})
        )
      );
    } else {
      this.store.dispatch([
        new SetValueInputTextAction(new KeyValue(this.field, this.model)),
        new SetSuccessInputTextAction(this.field)]);
      this.success.emit();
    }
  }
}
