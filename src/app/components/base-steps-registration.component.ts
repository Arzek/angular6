import { BaseComponent } from './base.component';
import { Input, Output, EventEmitter } from '../../../node_modules/@angular/core';

export class BaseStepsRegistrationComponent extends BaseComponent {
  @Input() step: number;
  @Output() finishStep: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
    super();
  }
}
