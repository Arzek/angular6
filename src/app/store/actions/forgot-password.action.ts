import { KeyValue } from '../../models/key-value';

export namespace ForgotPasswordActions {
  export class SetPhoneAction {
    static readonly type = '[FORGOT PASSWORD] Set phone';
    constructor(public payload: string) { }
  }
  export class SetSuccessPhoneAction {
    static readonly type = '[FORGOT PASSWORD] Set success phone';
    constructor(public payload: string) { }
  }
  export class SetErrorPhoneAction {
    static readonly type = '[FORGOT PASSWORD] Set error phone';
    constructor(public payload: number) { }
  }
  export class SetPasswortAction {
    static readonly type = '[FORGOT PASSWORD] Set password';
    constructor(public payload: KeyValue) { }
  }
  export class SetSuccessPasswordAction {
    static readonly type = '[FORGOT PASSWORD] Set success password';
    constructor(public payload: KeyValue) { }
  }
  export class SetErrorPasswordAction {
    static readonly type = '[FORGOT PASSWORD] Set error password';
    constructor(public payload: KeyValue) { }
  }
  export class CheckPasswordsAction {
    static readonly type = '[FORGOT PASSWORD] Check passwords';
  }
  export class CheckValidStepOneAction {
    static readonly type = '[FORGOT PASSWORD] Check step one';
  }
  export class SubmitStepOneAction {
    static readonly type = '[FORGOT PASSWORD] Submit step one';
  }
  export class SetSmsPasswordAction {
    static readonly type = '[FORGOT PASSWORD] Set sms password';
    constructor (public payload: string) { }
  }

  export class SetSuccessSmsPasswordAction {
    static readonly type = '[FORGOT PASSWORD] Set success sms password';
    constructor (public payload: string) { }
  }

  export class SetErrorSmsPasswordAction {
    static readonly type = '[FORGOT PASSWORD] Set error sms password';
    constructor (public payload: number | string | null) { }
  }

  export class SubmitSmsPasswordAction {
    static readonly type = '[FORGOT PASSWORD] Submit sms password';
    constructor (public payload: number) {}
  }

  export class ResetStoreAction {
    static readonly type = '[FORGOT PASSWORD] Reset store';
  }

  export class SuccessChagePasswordAction {
    static readonly type = '[FORGOT PASSWORD] Success chamge password';
  }
}






