import { ErrorResponse } from '../../models/error.response';

export class SetPhoneAction {
  static readonly type = '[LOGIN] Set phone';
  constructor (public payload: string) {}
}

export class SetErrorPhoneAction {
  static readonly type = '[LOGIN] Set error phone';
  constructor (public payload: number) {}
}

export class SetSuccessPhoneAction {
  static readonly type = '[LOGIN] Set success phone';
  constructor (public payload: string) {}
}

export class SetPasswortAction {
  static readonly type = '[LOGIN] Set password';
  constructor (public payload: string) {}
}

export class SetSuccessPasswordAction {
  static readonly type = '[LOGIN] Set success password';
  constructor (public payload: string) {}
}

export class SetErrorPasswordAction {
  static readonly type = '[LOGIN] Set error password';
  constructor (public payload: number) {}
}

export class GetNeedPassword {
  static readonly type = '[LOGIN] Get needPassword';
}

export class LoginPasswordAction {
  static readonly type = '[LOGIN] Login password';
}

export class LoginSuccessAction {
  static readonly type = '[LOGIN] Login success';
  constructor (public payload: any) {}
}

export class LoginErrorAction {
  static readonly type = '[LOGIN] Login error';
  constructor (public payload: Array<ErrorResponse>) {}
}
