import { ErrorModel } from '../../models/error-action.model';
import { KeyValue } from '../../models/key-value';

export class SetErrorInputTextAction {
  static readonly type = '[APP] Set error input';
  constructor (public payload: ErrorModel) { }
}

export class SetSuccessInputTextAction {
  static readonly type = '[APP] Set success input';
  constructor (public payload: string) { }
}

export class SetValueInputTextAction {
  static readonly type = '[APP] Set value input';
  constructor (public payload: KeyValue) { }
}

export class EnableProgressAction {
  static readonly type = '[APP] Enable progress';
}

export class DisableProgressAction {
  static readonly type = '[APP] Disable progress';
}

export class ResetStateAction {
  static readonly type = '[APP] Reset state';
}

export class SetItemAction {
  static readonly type = '[APP] Set item';
  constructor (public payload: KeyValue) {}
}
