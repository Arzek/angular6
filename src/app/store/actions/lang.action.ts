import { LangEntity } from '../../models/lang.entity';

export class ChangeLangAction {
  static readonly type = '[LANG] Change lang';
  constructor(public payload: LangEntity) { }
}
