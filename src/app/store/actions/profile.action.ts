export class GetProfileDataAction {
  static readonly type = '[PROFILE] Get profile data';
}

export class SetUserSubscribe {
  static readonly type = '[PROFILE] Set user subscribe';
}
