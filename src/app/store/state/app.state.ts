import { State, Action, StateContext } from '@ngxs/store';
import { InputText } from '../../models/input-text.store';
import { SetErrorInputTextAction,
  SetSuccessInputTextAction,
  SetValueInputTextAction,
  EnableProgressAction,
  DisableProgressAction,
  ResetStateAction,
  SetItemAction} from '../actions/app.action';
import { AppStore } from '../../models/app.store';
import { StepsSwitcherComponent } from '../../components/steps-switcher/steps-switcher.component';

@State<AppStore>({
  name: 'app',
  defaults: new AppStore()
})
export class AppState {

  @Action(SetErrorInputTextAction)
  setErrorInput({getState, setState}: StateContext<AppStore>, action: SetErrorInputTextAction) {
    const state = getState();
    if (action.payload.field in state) {

      if (action.payload.isBooleanError) {
        state[action.payload.field].valid.setErrorNoMassage();
      }

      if (action.payload.isCodeError) {
        state[action.payload.field].valid.setError(action.payload.code);
      }

      if (action.payload.isTextError) {
        state[action.payload.field].valid.setErrorMassage(action.payload.message);
      }
    }

    setState(state);
  }

  @Action(SetSuccessInputTextAction)
  setSuccessInput({getState, setState}: StateContext<AppStore>, action: SetSuccessInputTextAction) {
    const state = getState();
    if (action.payload in state) {
      state[action.payload].valid.clearError();
    }
    setState(state);
  }

  @Action(SetValueInputTextAction)
  setValueInput({getState, setState}: StateContext<AppStore>, action: SetValueInputTextAction) {
    const state = getState();
    if (action.payload.key in state) {
      state[action.payload.key].value = action.payload.value;
    }
    setState(state);
  }

  @Action(EnableProgressAction)
  enableProgress({getState, setState}: StateContext<AppStore>) {
    const state = getState();
    state.progress = true;
    setState(state);
  }

  @Action(DisableProgressAction)
  disableProgress({getState, setState}: StateContext<AppStore>) {
    const state = getState();
    state.progress = false;
    setState(state);
  }

  @Action(ResetStateAction)
  resetState({setState}: StateContext<AppStore>) {
    setState(new AppStore());
  }

  @Action(SetItemAction)
  setItem({getState, setState}: StateContext<AppStore>, action: SetItemAction) {
    const state = getState();
    if (action.payload.key in state) {
      state[action.payload.key] = action.payload.value;
      setState(state);
    }
  }
}
