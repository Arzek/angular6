import { State, Action, StateContext, Selector } from '@ngxs/store';
import { ProfileStore } from '../../models/profile.store';
import { GetProfileDataAction, SetUserSubscribe } from '../actions/profile.action';
import { ProfileService } from '../../services/profile.service';
import { Navigate } from '@ngxs/router-plugin';
import { CartService } from '../../services/cart.service';
import { CardHelper } from '../../helpers/card.helper';
import { EnableProgressAction, DisableProgressAction } from '../actions/app.action';
import { staticConfig } from '../../../config';

@State<ProfileStore>({
  name: 'profile',
  defaults: new ProfileStore()
})
export class ProfileState {
  constructor(private sevice: ProfileService,
    private cardServive: CartService,
    private cardHelper: CardHelper) { }
  @Action(GetProfileDataAction)
  async getData({ setState, getState, dispatch }: StateContext<ProfileStore>) {

    dispatch(new EnableProgressAction());
    this.sevice.getProfileData().then(res => {
      dispatch(new DisableProgressAction());
      const state = getState();
      for (const key in res['params']['required']) {
        if (key in res['params']['required']) {
          if (res['params']['required'][key]) {
            if (key !== 'mobile') {
              if (key in res) {
                if (!res[key]) {
                  dispatch(new Navigate(['/profile/edit']));
                }
              } else {
                dispatch(new Navigate(['/profile/edit']));
              }
            }
          }
        }
      }

      state.profileData.setData(res);
      setState(state);
    });

    dispatch(new EnableProgressAction());
    this.sevice.getAccountsData().then(res => {
      dispatch(new DisableProgressAction());
      const state = getState();
      state.accountsData.setData(res['accounts'][0]);
      setState(state);
    });

    dispatch(new EnableProgressAction());
    this.cardServive.getCards().then(res => {
      dispatch(new DisableProgressAction());
      const state = getState();
      state.cards = res;
      setState(state);
      if (staticConfig.requiredCard) {
        if (!state.cards.length || !this.cardHelper.valid(state.cards)) {
          dispatch(new Navigate(['/profile/card/add']));
        }
      }
    });

    dispatch(new EnableProgressAction());
    this.sevice.getQuestions().then(questions => {
      dispatch(new DisableProgressAction());
      const state = getState();
      state.questions = questions;
      state.questionsNew = questions.filter(item => {
        return item.answer === null;
      });
      state.countQuestions = state.questionsNew.length;
      state.questionsOld = questions.filter(item => {
        return item.answer || item.answer === 0;
      });
      setState(state);
    });
  }

  @Action(SetUserSubscribe)
  async setSubscribe() {
    await this.sevice.setSubscribe();
  }
}
