import { State, Action, StateContext } from '@ngxs/store';
import { InputText } from '../../models/input-text.store';
import { PhoneHelper } from '../../helpers/phone.helper';
import { ForgotPasswordActions } from '../actions/forgot-password.action';
import { SetPasswortAction } from '../actions/login.action';
import { staticConfig } from '../../../config';
import { KeyValue } from '../../models/key-value';
import { ForgotPasswordService } from '../../services/forgot-password.service';
import { Navigate } from '../../../../node_modules/@ngxs/router-plugin';

@State<any>({
  name: 'forgotPassword',
  defaults: {
    phone: new InputText(),
    passwordOne: new InputText(),
    passwordTwo: new InputText(),
    sms: new InputText(),
    validStepOne: false,
    step: 1,
    smsId: null
  }
})
export class ForgotPasswordState { 
  constructor(private phoneHelper: PhoneHelper, private service: ForgotPasswordService) {}

  /** Phone actions */
  @Action(ForgotPasswordActions.SetPhoneAction)
  setPhone({dispatch}: StateContext<any>, action: ForgotPasswordActions.SetPhoneAction) {
    this.phoneHelper.chackPhone(action.payload).then(phone => {
      dispatch(new ForgotPasswordActions.SetSuccessPhoneAction(phone));
    }).catch(codeError => {
      dispatch(new ForgotPasswordActions.SetErrorPhoneAction(codeError));
    });
  }

  @Action(ForgotPasswordActions.SetSuccessPhoneAction)
  setSuccessPhoneStatus({getState, setState, dispatch}: StateContext<any>, action: ForgotPasswordActions.SetSuccessPhoneAction) {
    const state = getState();
    state.phone.value = action.payload;
    state.phone.valid.clearError();
    setState(state);
    dispatch(new ForgotPasswordActions.CheckValidStepOneAction);
  }

  @Action(ForgotPasswordActions.SetErrorPhoneAction)
  setErrorStatusPhone({getState, setState, dispatch}: StateContext<any>, action: ForgotPasswordActions.SetErrorPhoneAction) {
    const state = getState();
    state.phone.valid.setError(action.payload);
    setState(state);
    dispatch(new ForgotPasswordActions.CheckValidStepOneAction);
  }

  /** Password actions */

  @Action(ForgotPasswordActions.SetPasswortAction)
  setPasswort({ dispatch }: StateContext<any>, action: ForgotPasswordActions.SetPasswortAction) {
    if (!action.payload.value || action.payload.value.length < staticConfig.minPassword) {
      dispatch(new ForgotPasswordActions.SetErrorPasswordAction(new KeyValue(action.payload.key, 127)));
    } else {
      dispatch([
        new ForgotPasswordActions.SetSuccessPasswordAction(action.payload),
        new ForgotPasswordActions.CheckPasswordsAction()]);
    }
  }

  @Action(ForgotPasswordActions.SetErrorPasswordAction)
  setErrorPasswordAction({ getState, setState, dispatch }: StateContext<any>, action: ForgotPasswordActions.SetErrorPasswordAction) {
    const state = getState();
    if (action.payload.key === 'passwordOne') {
      state.passwordOne.valid.setError(action.payload.value);
    } else if (action.payload.key === 'passwordTwo') {
      state.passwordTwo.valid.setError(action.payload.value);
    }
    setState(state);
    dispatch(new ForgotPasswordActions.CheckValidStepOneAction);
  }

  @Action(ForgotPasswordActions.SetSuccessPasswordAction)
  setSuccessPasswordAction({ getState, setState, dispatch}: StateContext<any>, action: ForgotPasswordActions.SetSuccessPasswordAction) {
    const state = getState();
    if (action.payload.key === 'passwordOne') {
      state.passwordOne.value = action.payload.value;
      state.passwordOne.valid.clearError();
    } else if (action.payload.key === 'passwordTwo') {
      state.passwordTwo.value = action.payload.value;
      state.passwordTwo.valid.clearError();
    }
    setState(state);
    dispatch(new ForgotPasswordActions.CheckValidStepOneAction);
  }

  @Action(ForgotPasswordActions.CheckPasswordsAction)
  chackPasswords({getState, dispatch}: StateContext<any>, action: ForgotPasswordActions.CheckPasswordsAction) {
    const state = getState();
    if (state.passwordOne.valid.status && state.passwordTwo.valid.status) {
      if (state.passwordOne.value !== state.passwordTwo.value) {
        dispatch([
          new ForgotPasswordActions.SetErrorPasswordAction(new KeyValue('passwordTwo', 126)),
        ]);
      }
    }
  }

  @Action(ForgotPasswordActions.CheckValidStepOneAction)
  validStepOne({ getState, setState }: StateContext<any>, action: ForgotPasswordActions.CheckValidStepOneAction) {
    const state = getState();
    if (state.passwordOne.valid.status && state.passwordTwo.valid.status && state.phone.valid.status) {
      state.validStepOne = true;
    } else {
      state.validStepOne = false;
    }
    setState(state);
  }

  @Action(ForgotPasswordActions.SubmitStepOneAction)
  submitStepOne({ getState, setState, dispatch }: StateContext<any>) {
    const state = getState();
    state.step = 2;
    setState(state);
    this.service.changePassword(state.phone.value, state.passwordOne.value).then((data) => {
      state.smsId = data.sms_id;
      setState(state);
    }).catch(error => {});
  }

  @Action(ForgotPasswordActions.SetSmsPasswordAction)
  setSmsPassword({dispatch}: StateContext<any>, action: ForgotPasswordActions.SetSmsPasswordAction) {
    if (action.payload) {
      dispatch(new ForgotPasswordActions.SetSuccessSmsPasswordAction(action.payload));
    } else {
      dispatch(new ForgotPasswordActions.SetErrorSmsPasswordAction(null));
    }
  }

  @Action(ForgotPasswordActions.SetSuccessSmsPasswordAction)
  setSuccessSmsPassword({ getState, setState, dispatch }: StateContext<any>, action: ForgotPasswordActions.SetSuccessSmsPasswordAction) {
    const state = getState();
    state.sms.value = action.payload;
    state.sms.valid.clearError();
    setState(state);
  }

  @Action(ForgotPasswordActions.SetErrorSmsPasswordAction)
  setErrorSmsPassword({ getState, setState, dispatch }: StateContext<any>, action: ForgotPasswordActions.SetErrorSmsPasswordAction) {
    const state = getState();
    if (action.payload) {
      state.sms.valid.setErrorMassage(action.payload);
    } else {
      state.sms.valid.setErrorMassage('');
    }
    setState(state);
  }

  @Action(ForgotPasswordActions.SubmitSmsPasswordAction)
  submitSmsPassword({ getState, setState, dispatch }: StateContext<any>, action: ForgotPasswordActions.SubmitSmsPasswordAction) {
    const state = getState();
    this.service.changePasswordConfirm(state.smsId, action.payload).then(data => {
      dispatch([
        new ForgotPasswordActions.ResetStoreAction(),
        new ForgotPasswordActions.SuccessChagePasswordAction()
      ]);
    }).catch(error => {
      dispatch(new ForgotPasswordActions.SetErrorSmsPasswordAction(error.error.data[0].message));
    });
  }

  @Action(ForgotPasswordActions.ResetStoreAction)
  resetStore({ setState }: StateContext<any>) {
    setState({
      phone: new InputText(),
      passwordOne: new InputText(),
      passwordTwo: new InputText(),
      sms: new InputText(),
      validStepOne: false,
      step: 1,
      smsId: null
    });
  }

  @Action(ForgotPasswordActions.SuccessChagePasswordAction)
  successChangePassword({dispatch}) {
    dispatch(new Navigate(['/login']));
  }
}
