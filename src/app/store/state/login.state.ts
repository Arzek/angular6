import { State, Action, StateContext, Select, Selector } from '@ngxs/store';
import {
  SetPhoneAction,
  SetErrorPhoneAction,
  SetSuccessPhoneAction,
  SetPasswortAction, SetSuccessPasswordAction,
  SetErrorPasswordAction,
  GetNeedPassword,
  LoginPasswordAction,
  LoginSuccessAction,
  LoginErrorAction} from '../actions/login.action';
import { ErrorInput } from '../../models/error-input.model';
import { staticConfig } from '../../../config';
import { LoginService } from '../../services/login.service';
import { CheckPhoneResponce } from '../../models/check-phone.response';
import { LoginStore } from '../../models/login.store';
import { NeedPasswordResponse } from '../../models/need-password.response';
import { Navigate } from '../../../../node_modules/@ngxs/router-plugin';
import { PhoneHelper } from '../../helpers/phone.helper';
import { InputText } from '../../models/input-text.store';
declare var UIkit: any;

@State<LoginStore>({
  name: 'login',
  defaults: {
    phone: new InputText(),
    password: new InputText(),
    needPassword: null
  }
})
export class LoginState {
  constructor(private loginService: LoginService, private phoneHelper: PhoneHelper) { }

  @Selector()
  static getNeedPassword(state: LoginState) {
    return state.needPassword;
  }

  @Action(SetPhoneAction)
  setPhone({ dispatch }: StateContext<LoginStore>, action: SetPhoneAction) {
    this.phoneHelper.chackPhone(action.payload).then(phone => {
      dispatch(new SetSuccessPhoneAction(phone));
    }).catch(error => {
      dispatch(new SetErrorPhoneAction(error));
    });
  }

  @Action(SetSuccessPhoneAction)
  setSuccessPhone({ getState, setState }: StateContext<LoginStore>, action: SetSuccessPhoneAction) {
    const state = getState();
    state.phone.value = action.payload;
    state.phone.valid.clearError();
    setState(state);
  }

  @Action(SetErrorPhoneAction)
  setErrorPhoneAction({ getState, setState }: StateContext<LoginStore>, action: SetErrorPhoneAction) {
    const state = getState();
    state.phone.valid.setError(action.payload);
    setState(state);
  }

  @Action(SetPasswortAction)
  setPasswort({ dispatch }: StateContext<LoginStore>, action: SetPasswortAction) {
    if (!action.payload || action.payload.length < staticConfig.minPassword) {
      dispatch(new SetErrorPasswordAction(127));
    } else {
      dispatch(new SetSuccessPasswordAction(action.payload));
    }
  }

  @Action(SetErrorPasswordAction)
  SetErrorPasswordAction({ getState, setState }: StateContext<LoginStore>, action: SetErrorPasswordAction) {
    const state = getState();
    state.password.valid.setError(action.payload);
    setState(state);
  }

  @Action(SetSuccessPasswordAction)
  setSuccessPassword({ getState, setState }: StateContext<LoginStore>, action: SetSuccessPasswordAction) {
    const state = getState();
    state.password.value = action.payload;
    state.password.valid.clearError();
    setState(state);
  }

  @Action(GetNeedPassword)
  needPassword(store: StateContext<LoginStore>) {
    this.loginService.getNeedPassword().then((data: NeedPasswordResponse) => {
      store.setState({
        ...store.getState(),
        needPassword: data.need_password
      });
    });
  }

  @Action(LoginPasswordAction)
  loginPassword({ getState, dispatch}: StateContext<LoginStore>, action: LoginPasswordAction) {
    const state = getState();
    this.loginService.loginPassword(state.phone.value, state.password.value).then((res) => {
      dispatch(new LoginSuccessAction(res));
    }).catch( error => {
      dispatch(new LoginErrorAction(error.error.data));
    });
  }

  @Action(LoginErrorAction)
  loginError({getState, setState}: StateContext<LoginStore>, action: LoginErrorAction) {
    const state = getState();
    if (action.payload.length) {
      for (const item of action.payload) {
        if (item.field === 'password') {
          state.password.valid.setErrorMassage(item.message);
        }
      }
    }
  }

  @Action(LoginSuccessAction)
  loginSuccessAction({ dispatch }: StateContext<LoginStore>, action: LoginSuccessAction) {
    localStorage.setItem('token', action.payload.token);
    dispatch(new Navigate(['/profile']));
  }
}
