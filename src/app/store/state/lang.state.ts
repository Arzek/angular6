import { State, Action, StateContext } from '@ngxs/store';
import { LangStore } from '../../models/lang.store';
import { LangModel } from '../../models/lang.model';
import { ChangeLangAction } from '../actions/lang.action';


@State<LangStore>({
  name: 'lang',
  defaults: {
    model: new LangModel()
  }
})
export class LangState {
  @Action(ChangeLangAction)
  change(store: StateContext<LangStore>, action: ChangeLangAction) {
    const state = store.getState();
    state.model.changeLang(action.payload);
    store.setState(state);
  }
}
