import { State, Action, StateContext } from '@ngxs/store';
import {ConfigService} from '../../services/config.service';
import {NeedPasswordResponse} from '../../models/need-password.response';
import {ConfigStore} from '../../models/config.store';

@State<ConfigStore>({
  name: 'config',
  defaults: {
    needPassword: null
  }
})
export class ConfigState {
  constructor(private configService: ConfigService) {}
}
