import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private http: HttpService) { }
  validPhone(phone: string) {
    return this.http.post('/client/check-phone', {phone}).toPromise();
  }

  getNeedPassword() {
    return this.http.get('/client/need-password', 'v2.1').toPromise();
  }

  loginPassword(phone: string, password: string) {
    return this.http.post('/client/auth-phone', {phone, password}, 'v2.1').toPromise();
  }

  loginPhone(phone: string) {
    return this.http.post('/client/auth-phone', {phone}, 'v2.1').toPromise();
  }
  phoneConfirm(phone: string, code: string, smsId: number) {
    return this.http.post('/client/auth-phone-confirm', {phone, code, sms_id: smsId}, 'v2.1').toPromise();
  }
}
