import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class PhoneService {

  constructor(private http: HttpService) {}

  validPhone(phone: string) {
    return this.http.post('/client/check-phone', {phone}).toPromise();
  }
}
