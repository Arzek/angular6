import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {NeedPasswordResponse} from '../models/need-password.response';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpService) { }
}
