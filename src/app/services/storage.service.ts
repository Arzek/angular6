import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setItem(key, value) {
    return Promise.resolve().then(() => {
      localStorage.setItem(key, value);
    });
  }
  getItem(key) {
    return Promise.resolve().then(() => {
      return localStorage.getItem(key);
    });
  }
}
