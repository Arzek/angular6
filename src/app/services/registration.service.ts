import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpService) { }

  registration(phone: string, password: string) {
    return this.http.post('/client/registration', { phone, password}, 'v2.1').toPromise();
  }
  registrationPhone(phone: string) {
    return this.http.post('/client/registration', { phone }, 'v2.1').toPromise();
  }
  registrationConfirm(code: string, smsId: number) {
    return this.http.post('/client/registration-confirm', { code, sms_id: smsId}, 'v2.1').toPromise();
  }

  editProfile(data: Object): Promise<any>  {
    data['channel_reg'] = 1;
    return this.http.put('/client/profile', data).toPromise();
  }
}
