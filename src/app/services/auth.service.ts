import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private stogare: StorageService, private store: Store) { }

  isAuth() {
    if (localStorage.getItem('token')) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem('token');
    this.store.dispatch(new Navigate(['/']));
  }


}
