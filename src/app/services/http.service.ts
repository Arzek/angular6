import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { staticConfig } from '../../config';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private apiUrl: string = staticConfig.apiUrl;
  private defaultVersion = 'v2';
  private headers: {};

  constructor(private httpClient: HttpClient) {
    this.headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*'
    };
  }

  get(url: string, version?: string) {
    if (!version) {
      version = this.defaultVersion;
    }

    return this.httpClient.get(this.getFullUrl(version, url), { headers: this.getHeaders() })
      .pipe(
        map(res => res['data'])
      );
  }

  post(url: string, data: Object, version?: string) {
    if (!version) {
      version = this.defaultVersion;
    }
    return this.httpClient.post(this.getFullUrl(version, url), this.getFormData(data), { headers: this.getHeaders() })
      .pipe(
        map(res => res['data'])
      );
  }

  put(url: string, data: Object, version?: string) {
    if (!version) {
      version = this.defaultVersion;
    }
    return this.httpClient.put(this.getFullUrl(version, url), this.getFormData(data), { headers: this.getHeaders() })
      .pipe(
        map(res => res['data'])
      );
  }

  private getFormData(data): string {
    const formBody = [];
    for (const property in data) {
      if (data[property]) {
        const encodedKey = encodeURIComponent(property);
        const encodedValue = encodeURIComponent(data[property]);
        formBody.push(encodedKey + '=' + encodedValue);
      }
    }
    return formBody.join('&');
  }

  private getFullUrl(version: string, url: string): string {
    return this.apiUrl + '/' + version + url;
  }

  private getHeaders() {
    const headers = this.headers;
    if (localStorage.getItem('lang')) {
      headers['Content-Language'] =  localStorage.getItem('lang');
      headers['Accept-Language'] =  localStorage.getItem('lang');
    }

    if (localStorage.getItem('token')) {
      headers['Authorization'] =  this.getToken(localStorage.getItem('token'));
    }

    return headers;
  }

  // tslint:disable
  getToken(token) {
    token = String(token).replace(/"|'/g, '');
    let preParsedToken = this.base64Encode(token + ':' + token);
    let parsedToken = String(preParsedToken).replace(/"|'/g, '');
    return String('Basic ' + parsedToken);
  }

  base64Encode( data ) {
    let b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    let o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

    do {
      o1 = data.charCodeAt(i++);
      o2 = data.charCodeAt(i++);
      o3 = data.charCodeAt(i++);

      
      bits = o1<<16 | o2<<8 | o3;

      h1 = bits>>18 & 0x3f;
      h2 = bits>>12 & 0x3f;
      h3 = bits>>6 & 0x3f;
      h4 = bits & 0x3f;

      enc += b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    switch( data.length % 3 ){
      case 1:
        enc = enc.slice(0, -2) + '==';
        break;
      case 2:
        enc = enc.slice(0, -1) + '=';
        break;
    }

    return enc;
  }
}
