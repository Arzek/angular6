import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private http: HttpService) { }
  validCart(cart) {
    return this.http.get(`/client/card/${cart}/availability`).toPromise();
  }
  setCart(cart: string) {
    return this.http.post('/client/card/set-card', {number: cart}).toPromise();
  }
  getCards(): Promise<any[]> {
    return this.http.get('/client/card/card-info').toPromise().then(res => {
      return res.cards;
    });
  }

  blockCard(card: number) {
    return this.http.post('/client/card/block-card', {number: card}).toPromise();
  }

}
