import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpService) { }
  changePassword(phone: string, password: string) {
    return this.http.post('/client/change-password', {
      phone: phone,
      password: password
    }, 'v2.1').toPromise();
  }

  changePasswordConfirm(smsId: number, code: number) {
    return this.http.post('/client/change-password-confirm', {
      sms_id: smsId,
      code: code
    }, 'v2.1').toPromise();
  }
}
