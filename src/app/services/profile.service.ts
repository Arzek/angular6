import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpService) { }

  getProfileData() {
    return this.http.get('/client/profile').toPromise();
  }

  getAccountsData() {
    return this.http.get('/client/accounts').toPromise();
  }

  getQuestions() {
    return this.http.get('/client/nps/list').toPromise().then(res => {
      return res.items.filter( item => {
        let cabinet = false;
        for (const field in item.communication_type) {
          if (item.communication_type[field] === 'cabinet') {
            cabinet = true;
          }
        }
        return (item.status === 1 || item.status === '1') && cabinet;
      });
    });
  }

  setQuestion(id: number, value: number) {
    return this.http.get(`/client/nps/vote/cabinet/${id}/${value}`).toPromise();
  }

  setSubscribe(): Promise<any> {
    return this.http.put('/client/profile', {
      email_subscribe: 1,
      sms_subscribe: 1
    }).toPromise();
  }
}
