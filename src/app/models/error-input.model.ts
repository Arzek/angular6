export class ErrorInput {
  status = false;
  messageCode: number = null;
  massageString: string = null;

  constructor(noRequired?: boolean) {
    if (noRequired) {
      this.status = true;
    }
  }
  setErrorNoMassage() {
    this.status = false;
    this.massageString = null;
    this.messageCode = null;
  }

  setErrorMassage(massage: string): void {
    this.status = false;
    this.massageString = massage;
    this.messageCode = null;
  }

  setError(code?: number): void {
    this.status = false;
    this.massageString = null;
    this.messageCode = code;
  }
  clearError() {
    this.status = true;
    this.messageCode = null;
    this.massageString = null;
  }
}
