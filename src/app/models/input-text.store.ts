import { ErrorInput } from './error-input.model';

export class InputText {
  value: string;
  valid: ErrorInput;

  constructor(noRequired?: boolean) {
    this.valid = new ErrorInput(noRequired);
  }
}
