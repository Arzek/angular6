import { InputText } from './input-text.store';

export class AppStore {

  needPassword = false;

  // login
  phone: InputText = new InputText();
  password: InputText = new InputText();
  progress = false;
  // forgot-password
  passwordOne: InputText = new InputText();
  passwordTwo: InputText = new InputText();


  sms: InputText = new InputText();

  // registration

  registrationLarge = false;
  step = 1;

  cart: InputText = new InputText();

}
