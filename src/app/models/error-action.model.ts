export class ErrorModel {

  field = null;
  isCodeError = false;
  isTextError = false;
  isBooleanError = false;

  code = null;
  message = null;

  constructor(data) {
    for (const field in data) {
      if (field in data && field in this) {
        this[field] = data[field];
      }
    }

  }
}
