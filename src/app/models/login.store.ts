import { ErrorInput } from './error-input.model';
import { InputText } from './input-text.store';

export class LoginStore {
  phone: InputText;
  password: InputText;
  needPassword: boolean = null;
}
