export class ErrorResponse {
  field: string;
  message: string;
}
