export class ProfileModel {
  data: Object;

  setData(data) {
    this.data = data;
  }

  getProfileItem(key) {
    if (this.data && key) {
      if (key in this.data) {
        if (this.data[key]) {
          return this.data[key];
        } else {
          return '';
        }
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  getFullName() {
    return `${this.getProfileItem('first_name')} ${this.getProfileItem('last_name')}`;
  }
}
