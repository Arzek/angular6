import { LangEntity } from './lang.entity';
import { translateTexts } from '../../translate';
import { staticConfig } from '../../config';

export class LangModel {
  currentLang: LangEntity;
  languages: LangEntity[] = [];
  constructor() {
    this.languages = staticConfig.lang.languages.map( item => {
      return new LangEntity(item);
    });

    if (!localStorage.getItem('lang')) {
      this.currentLang = staticConfig.lang.currentLang;
    } else {
      const res = this.languages.find(lang => lang.header === localStorage.getItem('lang'));
      if (res) {
        this.currentLang = res;
      } else {
        this.currentLang = staticConfig.lang.currentLang;
      }
    }


    this.onChange();
  }

  changeLang(currentLang) {
    this.currentLang = currentLang;
    this.onChange();
  }

  getText(alias: any, replaceObject?: Object): string {
    if (translateTexts && this.currentLang) {
      if (this.currentLang.alias in translateTexts) {
        if (alias in translateTexts[this.currentLang.alias]) {
          if (!replaceObject) {
            return translateTexts[this.currentLang.alias][alias];
          } else {
            return translateTexts[this.currentLang.alias][alias].replace(replaceObject['name'], replaceObject['value']);
          }
        }
      }
    }
    return '';
  }

  private onChange() {
    localStorage.setItem('lang', this.currentLang.header);
  }
}
