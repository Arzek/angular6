export class LangEntity {
  name: string;
  alias: string;
  header: string;
  constructor(data: LangEntity) {
    this.name = data.name;
    this.alias = data.alias;
    this.header = data.header;
  }
}
