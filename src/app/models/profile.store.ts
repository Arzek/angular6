import { ProfileModel } from './profile.model';

export class ProfileStore {
  profileData: ProfileModel = new ProfileModel();
  accountsData: ProfileModel = new ProfileModel();
  cards: any[] = [];

  questions: any[] = [];
  countQuestions = 0;
  questionsNew: any[] = [];
  questionsOld: any[] = [];
}
