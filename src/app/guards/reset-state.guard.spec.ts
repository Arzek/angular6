import { TestBed, async, inject } from '@angular/core/testing';

import { ResetStateGuard } from './reset-state.guard';

describe('ResetStateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResetStateGuard]
    });
  });

  it('should ...', inject([ResetStateGuard], (guard: ResetStateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
