import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { Select, Store } from '@ngxs/store';
import { LoginState } from '../store/state/login.state';
import { map } from '../../../node_modules/rxjs/operators';
import { Navigate } from '../../../node_modules/@ngxs/router-plugin';
import { SetItemAction } from '../store/actions/app.action';
import { KeyValue } from '../models/key-value';

@Injectable({
  providedIn: 'root'
})
export class NeedPasswordGuard implements CanActivate {
  constructor (private loginService: LoginService, private store: Store) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.loginService.getNeedPassword().then(data => {
        this.store.dispatch(new SetItemAction(new KeyValue('needPassword', data.need_password)));
        return true;
      });
  }
}
