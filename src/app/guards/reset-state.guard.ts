import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { ResetStateAction } from '../store/actions/app.action';

@Injectable({
  providedIn: 'root'
})
export class ResetStateGuard implements CanActivate {

  constructor(private store: Store) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      this.store.dispatch(new ResetStateAction());
      return true;
  }
}
