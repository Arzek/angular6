import { TestBed, async, inject } from '@angular/core/testing';

import { NeedPasswordGuard } from './need-password.guard';

describe('NeedPasswordGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NeedPasswordGuard]
    });
  });

  it('should ...', inject([NeedPasswordGuard], (guard: NeedPasswordGuard) => {
    expect(guard).toBeTruthy();
  }));
});
