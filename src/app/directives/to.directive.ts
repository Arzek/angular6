import { Directive, HostListener, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';

@Directive({
  selector: '[appTo]'
})
export class ToDirective {
  @Input() appTo: string;
  constructor(private store: Store) { }
  @HostListener('click')
  onClick () {
    this.store.dispatch(new Navigate([this.appTo]));
  }
}
