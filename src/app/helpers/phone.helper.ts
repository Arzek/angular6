import { Injectable } from '@angular/core';
import { staticConfig } from '../../config';
import { LoginService } from '../services/login.service';
import { CheckPhoneResponce } from '../models/check-phone.response';


@Injectable({
  providedIn: 'root'
})
export class PhoneHelper {
  constructor (private loginService: LoginService) {}
  chackPhone(phone: string): Promise<any> {

    // tslint:disable-next-line:no-shadowed-variable
    return new Promise((resolve, reject) => {
      if (staticConfig.phone.regex.test(phone)) {
        this.loginService.validPhone(phone).then((data: CheckPhoneResponce) => {
          if (data.is_exist) {
            resolve(phone);
          } else {
            reject(106);
          }
        }).catch(error => {
          reject(error.error.data[0].message);
        });
      } else {
        reject(130);
      }
    });
  }
}
