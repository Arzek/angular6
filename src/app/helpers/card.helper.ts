
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CardHelper {
  valid(cards): boolean   {
    const res = cards.filter((t) => {
      if (String(t.number).indexOf('200') !== 0 && (t.status === 1 || t.status === 3) ) {
        return false;
      } else {
        return true;
      }
    });

    const count = cards.length - res.length;
    if (!count) {
      return false;
    } else {
      return true;
    }
  }
}