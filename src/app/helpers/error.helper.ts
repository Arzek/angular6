
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class ErrorHelper {
  getMassege(error): string {
    return error.error.data[0].message;
  }
}
