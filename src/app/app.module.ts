import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LanguageWidgetComponent } from './components/language-widget/language-widget.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegistrationPageComponent } from './components/registration-page/registration-page.component';

import { LangState } from './store/state/lang.state';
import {ConfigState} from './store/state/config.state';
import {HttpClientModule} from '@angular/common/http';
import { LoginPasswordComponent } from './components/login-password/login-password.component';
import { PhoneInputComponent } from './components/phone-input/phone-input.component';
import { PasswordInputComponent } from './components/password-input/password-input.component';
import {FormsModule} from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { LoginState } from './store/state/login.state';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { ForgotPasswordPageComponent } from './components/forgot-password-page/forgot-password-page.component';
import { ForgotPasswordState } from './store/state/forgot-password.state';
import { SmsComponent } from './components/sms/sms.component';
import { CartPageComponent } from './components/cart-page/cart-page.component';
import { AddCartPageComponent } from './components/add-cart-page/add-cart-page.component';
import { MailingPageComponent } from './components/mailing-page/mailing-page.component';
import { FeedbackPageComponent } from './components/feedback-page/feedback-page.component';

import { BsDatepickerModule } from 'ngx-bootstrap';
import { InputDatepickerComponent } from './components/input-datepicker/input-datepicker.component';
import { FullRegistrationComponent } from './components/full-registration/full-registration.component';
import { OneStepRegistrationComponent } from './components/one-step-registration/one-step-registration.component';
import { TwoStepRegistrationComponent } from './components/two-step-registration/two-step-registration.component';
import { StepsSwitcherComponent } from './components/steps-switcher/steps-switcher.component';
import { CartInputComponent } from './components/cart-input/cart-input.component';
import { NewsPageComponent } from './components/news-page/news-page.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { AppState } from './store/state/app.state';
import { ToDirective } from './directives/to.directive';
import { InputTextComponent } from './components/input-text/input-text.component';
import { InputSelectComponent } from './components/input-select/input-select.component';
import { InputRegionComponent } from './components/input-region/input-region.component';
import { InputCityComponent } from './components/input-city/input-city.component';

import { defineLocale } from 'ngx-bootstrap/chronos';

import { InputCheckboxComponent } from './components/input-checkbox/input-checkbox.component';
import { LoginSmsComponent } from './components/login-sms/login-sms.component';
import { ProfileHeadComponent } from './components/profile-head/profile-head.component';
import { ProfileSubheadComponent } from './components/profile-subhead/profile-subhead.component';
import { ProfileState } from './store/state/profile.state';
import { ProfileEditPageComponent } from './components/profile-edit-page/profile-edit-page.component';
import { ProfileSlidersComponent } from './components/profile-sliders/profile-sliders.component';
import { ProfileSliderComponent } from './components/profile-slider/profile-slider.component';
import { ActionsPageComponent } from './components/actions-page/actions-page.component';
import { PurchaseHistoryComponent } from './components/purchase-history/purchase-history.component';
import { InputRangepickerComponent } from './components/input-rangepicker/input-rangepicker.component';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { ruLocale, enGbLocale } from 'ngx-bootstrap/locale';
import { ukLocale } from '../uk';
import { QuestionComponent } from './components/question/question.component';
import { OffcanvasComponent } from './components/offcanvas/offcanvas.component';
import { PurchaseStatisticsPageComponent } from './components/purchase-statistics-page/purchase-statistics-page.component';
import { ChartsModule } from 'ng2-charts';
import { CategoryComponent } from './components/category/category.component';
import { ProductComponent } from './components/product/product.component';

defineLocale('ru', ruLocale);
defineLocale('eng', enGbLocale);
defineLocale('uk', ukLocale);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LanguageWidgetComponent,
    WelcomeComponent,
    LoginPageComponent,
    RegistrationPageComponent,
    LoginPasswordComponent,
    PhoneInputComponent,
    PasswordInputComponent,
    ProfilePageComponent,
    ForgotPasswordPageComponent,
    SmsComponent,
    CartPageComponent,
    AddCartPageComponent,
    MailingPageComponent,
    FeedbackPageComponent,
    InputDatepickerComponent,
    FullRegistrationComponent,
    OneStepRegistrationComponent,
    TwoStepRegistrationComponent,
    StepsSwitcherComponent,
    CartInputComponent,
    NewsPageComponent,
    SpinnerComponent,
    ToDirective,
    InputTextComponent,
    InputSelectComponent,
    InputRegionComponent,
    InputCityComponent,
    InputCheckboxComponent,
    LoginSmsComponent,
    ProfileHeadComponent,
    ProfileSubheadComponent,
    ProfileEditPageComponent,
    ProfileSlidersComponent,
    ProfileSliderComponent,
    ActionsPageComponent,
    PurchaseHistoryComponent,
    InputRangepickerComponent,
    QuestionComponent,
    OffcanvasComponent,
    PurchaseStatisticsPageComponent,
    CategoryComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgxsModule.forRoot([
      LangState,
      LoginState,
      ForgotPasswordState,
      AppState,
      ProfileState
    ]),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    HttpClientModule,
    TextMaskModule,
    BsDatepickerModule.forRoot(),
    AngularSvgIconModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
