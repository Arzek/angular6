import { Component, ChangeDetectorRef } from '@angular/core';
import {Store, Select} from '@ngxs/store';
import { Meta, Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { AppStore } from './models/app.store';
import { staticConfig } from '../config';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { lang } from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @Select(state => state.app) appStore$: Observable<AppStore>;
  constructor (private localeService: BsLocaleService, private title: Title, private cdr: ChangeDetectorRef) {
    this.title.setTitle(staticConfig.title);
    // this.meta.removeTag('name="viewport"');
    // this.meta.addTag({ name: 'viewport', content: 'width=1366, initial-scale=1' });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
}
