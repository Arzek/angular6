import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegistrationPageComponent } from './components/registration-page/registration-page.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { AuthGuard } from './guards/auth.guard';
import { ForgotPasswordPageComponent } from './components/forgot-password-page/forgot-password-page.component';
import { NeedPasswordGuard } from './guards/need-password.guard';
import { CartPageComponent } from './components/cart-page/cart-page.component';
import { AddCartPageComponent } from './components/add-cart-page/add-cart-page.component';
import { MailingPageComponent } from './components/mailing-page/mailing-page.component';
import { FeedbackPageComponent } from './components/feedback-page/feedback-page.component';
import { NewsPageComponent } from './components/news-page/news-page.component';
import { ResetStateGuard } from './guards/reset-state.guard';
import { ProfileGuard } from './guards/profile.guard';
import { ProfileEditPageComponent } from './components/profile-edit-page/profile-edit-page.component';
import { ActionsPageComponent } from './components/actions-page/actions-page.component';
import { PurchaseStatisticsPageComponent } from './components/purchase-statistics-page/purchase-statistics-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', canActivate: [ResetStateGuard], component: LoginPageComponent },
  { path: 'forgot-password', canActivate: [ResetStateGuard, NeedPasswordGuard], component: ForgotPasswordPageComponent },
  { path: 'registration', canActivate: [ResetStateGuard, NeedPasswordGuard], component: RegistrationPageComponent},

  { path: 'profile', canActivate: [AuthGuard, ProfileGuard], component: ProfilePageComponent},
  { path: 'profile/edit', canActivate: [AuthGuard, ProfileGuard], component: ProfileEditPageComponent},
  { path: 'profile/card', canActivate: [AuthGuard, ProfileGuard], component: CartPageComponent},
  { path: 'profile/card/add', canActivate: [AuthGuard, ProfileGuard], component: AddCartPageComponent},
  { path: 'profile/mailing', canActivate: [AuthGuard, ProfileGuard], component: MailingPageComponent},
  { path: 'profile/feedback', canActivate: [AuthGuard, ProfileGuard], component: FeedbackPageComponent},
  { path: 'profile/news/:partnerId/:id', canActivate: [AuthGuard, ProfileGuard], component: NewsPageComponent},
  { path: 'profile/actions/:id', canActivate: [AuthGuard, ProfileGuard], component: ActionsPageComponent},
  { path: 'profile/purchase-statistics', canActivate: [AuthGuard], component: PurchaseStatisticsPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
